# TinyCOV

This is a coverage collection agent that works similarly to DynamoRIO. The main differences are:

* Size. tinycov.c is just 1.5k lines long.
* Portability. Although it uses Linux signals, it can be easily adapted for other OS's exception handling.
* Speed. The recompilation is brain-dead, it runs orders of magnitude **slower** than proper alternatives. Current record is about 50 times slower than native, the objective is about 5-10 times slower.
* Purpose. This software has a single purpose of collecting code coverage, it does nothing else.

## A bit on inner workings, for tinkerers

This software repurposes the X86 trap flag to indicate that coverage has to be collected. When it receives a debug exception (int1 or int3), it will attempt to recompile the current code block into the equivalent code that does collect coverage, then jump to it with TF clear. To exit the tracing mode, a magic instruction (`nopl rsp, [rip+0x12345678]`, normally a no-op) can be used. To see how the coverage is set up, refer to `setup()`.

The following assumptions are made about the target program:

* The code does not change at runtime (i.e.no JIT).
* The code does not care about stack contents below the red zone (rsp-128).
* The code does not care about stack contents below rsp when returning from a function.
* The code does not use signals or other OS features that can actively interfere with TinyCOV.

Two functions, `jit_out` and `jit_in`, are used to switch between traced and untraced modes. `jit_out` is used to translate a "real" ucontext (rip = jit-generated) to a "logical" one (rip = original), and `jit_in` translates the other way, generating a new translation if necessary.

This design lets TinyCOV run most code more or less efficiently, and still be able to run an unknown instruction by single-stepping over it.

## Preprocessor macros

* `TRACE=1` enables trace mode. In this mode, dumps of register state and current instruction are made before each recompiled instruction's execution. Very slow
* `DEBUG_PRINTS=1` enables debug logging from the JIT recompiler. Only useful if you're hunting a bug in TinyCOV.
* `MAX_COUNT=n` enables a limit of `n` translated instructions. After that the trace mode is automatically disabled. Requires `TRACE=1`.
* `TRACE_SILENT=1` disables logging from `TRACE=1`. Only useful in conjunction with `MAX_COUNT=n`.
* `DEBUG_AFTER=n` enables debug logging (and trace logging, whatever is enabled) after `n` translated instructions. Requires `TRACE=1`.
* `TEST_MODE=1` makes the JIT entry a `void setup(void);`. By default an anonymous constructor is used to bootstrap the JIT.
* `SETSID=1` makes the JIT entry a `int setsid(void);`. This is useful for debugging/collecting coverage of isolated code fragments in high-level languages, which likely already have FFI wrappers for `setsid`.
* `LINUX_SYSCALL_HOOK=1` prevents application's use of certain syscalls that interfere with TinyCOV.
* `SKIP_HEADERS=1` prevents the inclusion of standard headers. Use this if you want to integrate this into a non-standard build system.
* `LINUX_IDT_WORKAROUND=1` (tinycov-kernel-base.c only) prevents recompilation of IDT entries 0x14 to 0x20 (they point to unmapped memory).
