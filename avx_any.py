import PIL.Image, capstone, concurrent.futures

def pack(a, b):
    return bytes((0xc4, ((a >> 2) & 0xe0) | ((b >> 9) + 1), ((b >> 1) & 0x80) | (a & 0x7f), b & 0xff))

def check(a, b):
    if (b == 0x12 or b == 0x16) and a % 8 < 2: return True
    if (b >= 0x14 and b < 0x16) and a % 4 < 2: return True
    if (b == 0x2a or b == 0x51 or b == 0x5a) and a % 4 >= 2: return True
    if (b >= 0x52 and b < 0x54) and a % 4 == 2: return True
    if ((b >= 0x54 and b < 0x58) or (b == 0xc6)) and a % 4 < 2: return True
    if (b >= 0x58 and b < 0x5a) or (b >= 0x5c and b < 0x60) or b == 0xc2: return True
    if ((b >= 0x60 and b < 0x6e) or (b >= 0x74 and b < 0x77) or (b >= 0xd1 and b < 0xd6) or (b >= 0xd8 and b < 0xe6) or (b >= 0xe8 and b < 0xff and b != 0xf0 and b != 0xf7) or (b >= 0x200 and b < 0x20e) or (b >= 0x228 and b < 0x230 and b != 0x22a) or (b >= 0x237 and b < 0x241) or (b >= 0x245 and b < 0x248) or (b >= 0x28c and b < 0x290 and b % 2 == 0) or (b >= 0x290 and b < 0x294) or (b >= 0x296 and b < 0x2c0 and b % 16 >= 6)) and a % 4 == 1: return True
    if ((b >= 0x7c and b < 0x7e) or (b == 0xd0)) and a % 2 == 1: return True
    if ((b == 0xc4) or (b >= 0x2dc and b < 0x2e0)) and a % 8 == 1: return True
    if (b == 0x216 or b == 0x236) and a % 8 == 5: return True
    if b == 0x2f2 and a % 8 == 0: return True
    if b == 0x2f5 and a % 8 < 4 and a % 8 != 1: return True
    if b == 0x2f6 and a % 8 == 3: return True
    if b == 0x2f7 and a % 8 < 4: return True
    if b % 0x200 < 0x100 and a % 128 < 120: return False
    if b >= 0x10 and b < 0x12: return True
    if ((b == 0x12) or (b >= 0x2c and b < 0x2e)) and a % 4 >= 2: return True
    if (b == 0x13 or b == 0x17 or (b >= 0x90 and b < 0x92 and a >= 0x200)) and a % 8 < 2: return True
    if b == 0x16 and a % 4 == 2: return True
    if ((b >= 0x28 and b < 0x2a) or (b == 0x2b) or (b >= 0x2e and b < 0x30) or (b == 0x51) or (b == 0x5a)) and a % 4 < 2: return True
    if ((b >= 0x52 and b < 0x54) or b == 0x77) and a % 4 == 0: return True
    if b == 0x5b and a % 4 < 3: return True
    if ((b == 0x6e) or (b == 0xd6) or (b == 0xe7) or (b == 0x241) or (b == 0x2db)) and a % 8 == 1: return True
    if (b == 0x6f or b == 0x7f) and (a % 4 == 1 or a % 4 == 2): return True
    if (b == 0x70 or b == 0xe6) and a % 4 > 0: return True
    if b == 0x7e and (a % 8 == 1 or a % 8 == 2): return True
    if ((b == 0xe7) or (b == 0x216) or (b >= 0x219 and b < 0x21b)) and a % 8 == 5: return True
    if b == 0xf0 and a % 4 == 3: return True
    if (b >= 0x190 and b < 0x192) and a >= 0x200 and a % 128 >= 120 and a % 128 < 122: return False
    if ((b >= 0x20e and b < 0x210) or (b == 0x213) or (b >= 0x217 and b < 0x219) or (b >= 0x21c and b < 0x226 and b != 0x21f) or (b == 0x22a) or (b >= 0x230 and b < 0x236) or (b >= 0x258 and b < 0x25a) or (b >= 0x278 and b < 0x27a)) and a % 4 == 1: return True
    if b == 0x216 and a % 8 == 1: return False
    if b % 0x200 >= 0x100: return check(a, b-0x100)
    return False

def check_regreg(a, b):
    f = check(a, b)
    if ((b >= 0x41 and b < 0x43) or (b >= 0x45 and b < 0x48)) and a >= 0x200 and (a % 0x100) >= 0xc0 and (a % 8 == 4 or a % 8 == 5): return True
    if b == 0x91 and a >= 0x200 and a % 128 >= 120 and a % 8 < 2: return False
    if b == 0x90 and a >= 0x200 and a % 256 >= 120 and a % 256 < 122: return False
    if (b == 0x44 or b == 0x98) and a >= 0x200 and a % 256 >= 248 and a % 256 < 250: return True
    if b < 0x200:
        b %= 0x100
        if (b >= 0x10 and b < 0x12) and a % 4 >= 2: return True
        if (b == 0x12 or b == 0x16) and a % 8 == 1: return False
        if b == 0x4b and a >= 0x200 and (a % 0x100) >= 0xc0 and a % 8 == 5: return True
        if a % 128 < 120: return f
        if b == 0x13 or (b == 0x17 and a % 8 < 2): return False
        if b == 0x2b and a % 4 < 2: return False
        if b == 0x50 and a % 4 < 2: return True
        if b == 0x93 and a % 0x100 >= 0x80 and a % 8 < 2: return True
        if b == 0x92 and a >= 0x200 and a % 8 < 2: return True
        if (b == 0xc5 or b == 0xf7) and a % 8 == 1: return True
        if b == 0xd7 and a % 4 == 1: return True
        if b == 0xe7 and a % 4 == 1: return False
        if b == 0xf0 and a % 4 == 3: return False
    else:
        b %= 0x100
        if (b >= 0x2c and b < 0x30) and a % 4 == 1: return False
        if ((b == 0x8c) or (b == 0x8e) or (b >= 0x90 and b < 0x94)) and a % 4 == 1: return False
        if a % 128 <= 120: return f
        if b == 0x1a and a % 8 == 5: return False
        if b == 0x2a and a % 4 == 1: return False
    return f

def test(q):
    a, b = q
    bb = pack(a, b) + b'\xc0' * 32
    try: next(cs.disasm(bb, 0))
    except StopIteration: valid = False
    else: valid = True
    return valid

if __name__ == '__main__':
    cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
    img = PIL.Image.new('RGBA', (1024, 1024))
    right = 0
    wrong = 0
    keys = [(a, b) for a in range(1024) for b in range(1024)]
    for ((a, b), valid) in zip(keys, map(test, keys)):
        chk = check_regreg(a, b)
        if chk != valid:
            img.putpixel((a, b), (255, 0, 0))
            wrong += 1
        else:
            img.putpixel((a, b), (0, 255, 0))
            right += 1
    print(right, 'right,', wrong, 'wrong')
    img.save('avx_any.png')
