from avx_any import check, check_regreg, pack

import PIL.Image, capstone

img = PIL.Image.new('RGBA', (512, 512))
cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)

for i in range(1024):
    ss = set()
    qq = []
    for j in range(1024):
        if check(j, i):
            bb = pack(j, i) + b'\x40\xb3'*6
        elif check_regreg(j, i):
            bb = pack(j, i) + b'\xc0'*12
        else:
            continue
        try: instr = next(cs.disasm(bb, 0))
        except StopIteration: assert False, bb
        qq.append((j, i, instr))
        ss.add(len(instr.bytes))
    if len(ss) != 1:
        if not ss:
            color = (0, 255, 255)
        else:
            print('#######')
            for j in qq: print(*j)
            color = (0, 0, 0)
    else:
        ss, = ss
        if ss == 4:
            color = (0, 255, 0)
        elif ss == 5:
            color = (0, 0, 255)
        elif ss == 6:
            color = (255, 255, 0)
        elif ss == 7:
            color = (255, 255, 255)
        else:
            color = (255, 0, 0)
    for x in range(16):
        for y in range(16):
            if x == 15 or y == 15:
                img.putpixel((16*(i//32)+x, 16*(i%32)+y), (0, 0, 0))
            else:
                img.putpixel((16*(i//32)+x, 16*(i%32)+y), color)

img.save('avx_any_format.png')
