import capstone, PIL.Image

from avx_c5 import check, check_regreg, check1_regreg

cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)

def exists(bb):
    q = cs.disasm(bb, 0)
    try: next(q)
    except StopIteration: return False
    return True

def check1_regreg(first, second):
    f = check_regreg(first, second)
    if (second in range(0x41, 0x43) or second in range(0x45, 0x48)) and first >= 0xc0 and first % 8 in range(4, 6): return not f
    if second == 0x4b and first >= 0xc0 and first % 8 == 5: return not f
    if (second == 0x44 or second == 0x90 or second == 0x93 or second == 0x98) and first in range(0xf8, 0xfa): return not f
    if second == 0x93 and first in range(0x78, 0x7a): return not f
    if second == 0xc4 and first % 128 == 121: return not f
    return f

for a in range(256):
    for b in range(256):
        bb = bytes((0xc4, (a & 128) | 1, a & 127, b))
        c1 = exists(bb+bytes(12))
        c2 = exists(bb+b'\xcc'*12)
        assert c1 == check(a, b)
        assert c2 == check1_regreg(a, b)
