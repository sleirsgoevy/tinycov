import capstone, PIL.Image

def check(first, second):
    if first in range(0xf8, 0xfa) and second in range(0x90, 0x92): return True
    if first % 128 >= 120:
        if second in range(0x10, 0x12): return True
        if second == 0x12 and first % 4 >= 2: return True
        if (second == 0x13 or second == 0x17) and first % 8 < 2: return True
        if second == 0x16 and first % 4 == 2: return True
        if (second in range(0x28, 0x2a) or second == 0x2b or second in range(0x2e, 0x30)) and first % 4 < 2: return True
        if (second == 0x2a or second in range(0x2c, 0x2e)) and first % 4 >= 2: return True
        if second == 0x51 and first % 4 < 2: return True
        if second in range(0x52, 0x54) and first % 4 == 0: return True
        if second == 0x5a and first % 4 < 2: return True
        if second == 0x5b and first % 4 < 3: return True
        if second == 0x6e and first % 8 == 1: return True
        if second == 0x6f and first % 4 in (1, 2): return True
        if second == 0x70 and first % 4 > 0: return True
        if second == 0x77 and first % 4 == 0: return True
        if second == 0x7e and first % 8 in (1, 2): return True
        if second == 0x7f and first % 4 in (1, 2): return True
        if second == 0xd6 and first % 8 == 1: return True
        if second == 0xe6 and first % 4 > 0: return True
        if second == 0xe7 and first % 4 == 1: return True
        if second == 0xf0 and first % 4 == 3: return True
    if second in range(0xd6, 0xd8) or second in range(0xe6, 0xe8) or second == 0xf0 or second == 0xf7 or second == 0xff: return False
    if second == 0x12 or second == 0x16: return first % 8 < 2
    if second in range(0x14, 0x16) or second in range(0x54, 0x58) or second == 0xc6: return first % 4 < 2
    if second in range(0x52, 0x54): return first % 4 == 2
    if second in range(0x58, 0x5a) or second in range(0x5c, 0x60) or second == 0xc2: return True
    if second == 0x2a or second == 0x51 or second == 0x5a: return first % 4 >= 2
    if second in range(0x60, 0x6e) or second in range(0x74, 0x77): return first % 4 == 1
    if second in range(0x7c, 0x7e) or second == 0xd0: return first % 2 == 1
    if second == 0xc4: return first % 8 == 1
    if second >= 0xd0: return first % 4 == 1
    return False

def check_regreg(first, second):
    f = check(first, second)
    if second in range(0x10, 0x12) and first % 128 < 120 and first % 4 >= 2: return not f
    if (second == 0x12 or second == 0x16) and first % 8 == 1: return not f
    if (second in range(0x41, 0x43) or second in range(0x45, 0x48)) and first >= 0xc0 and first % 8 in range(4, 6): return not f
    if second == 0x4b and first >= 0xc0 and first % 8 == 5: return not f
    if first % 128 < 120: return f
    if (second == 0x13 or second == 0x17 or second == 0x93) and first % 8 < 2: return not f
    if (second == 0x2b or second == 0x50) and first % 4 < 2: return not f
    if (second == 0x44 or second in range(0x91, 0x93) or second == 0x98) and first in range(0xf8, 0xfa): return not f
    if (second in range(0xc4, 0xc6) or second == 0xf7) and first % 8 == 1: return not f
    if (second == 0xd7 or second == 0xe7) and first % 4 == 1: return not f
    if second == 0xf0 and first % 4 == 3: return not f
    return f

def check1_regreg(first, second):
    f = check_regreg(first, second)
    if (second in range(0x41, 0x43) or second in range(0x45, 0x48)) and first >= 0xc0 and first % 8 in range(4, 6): return not f
    if second == 0x4b and first >= 0xc0 and first % 8 == 5: return not f
    if (second == 0x44 or second == 0x90 or second == 0x93 or second == 0x98) and first in range(0xf8, 0xfa): return not f
    if second == 0x93 and first in range(0x78, 0x7a): return not f
    if second == 0xc4 and first % 128 == 121: return not f
    return f

if __name__ == '__main__':
    img = PIL.Image.new('RGBA', (256, 256))
    cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
    for a in range(256):
        for b in range(256):
            instr = bytes((0xc4, (a & 128) | 0x61, a | 128, b))+b'\xc0'*13
            try: instr = next(cs.disasm(instr, 0))
            except StopIteration:
                valid = False
            else:
                valid = True
            chk = check1_regreg(a, b)
            if isinstance(chk, bool) and chk == valid:
                img.putpixel((a, b), (0, 255, 0))
            elif valid:
                img.putpixel((a, b), (0, 0, 0))
            else:
                img.putpixel((a, b), (255, 0, 0))
    img.save('avx_c5.png')
