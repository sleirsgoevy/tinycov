from avx_c5 import check, check_regreg

import PIL.Image, capstone

img = PIL.Image.new('RGBA', (256, 256))
cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)

for i in range(256):
    ss = set()
    for j in range(256):
        if check(j, i):
            bb = bytes((0xc5, j, i)) + b'\x40\xb3'*6
        elif check_regreg(j, i):
            bb = bytes((0xc5, j, i)) + b'\xc0'*12
        else:
            continue
        try: instr = next(cs.disasm(bb, 0))
        except StopIteration: assert False, bb
        ss.add(len(instr.bytes))
    if len(ss) != 1:
        if not ss:
            color = (0, 255, 255)
        else:
            color = (0, 0, 0)
    else:
        ss, = ss
        if ss == 3:
            color = (0, 255, 0)
        elif ss == 4:
            color = (0, 0, 255)
        elif ss == 5:
            color = (255, 255, 0)
        elif ss == 6:
            color = (255, 255, 255)
        else:
            color = (255, 0, 0)
    for x in range(16):
        for y in range(16):
            if x == 15 or y == 15:
                img.putpixel((16*(i//16)+x, 16*(i%16)+y), (0, 0, 0))
            else:
                img.putpixel((16*(i//16)+x, 16*(i%16)+y), color)

img.save('avx_c5_format.png')
