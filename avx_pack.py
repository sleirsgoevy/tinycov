from avx_any import pack

def unpack3(a, b, c):
    return (a & 0xe0) << 2 | (b & 0x7f), ((a & 0x1f) - 1) << 9 | (b & 0x80) << 1 | c

def unpack2(a, b):
    return (a & 0x80) << 2 | 0x180 | a, b

def c2to3(a, b):
    return bytes((0xc4, (a & 0x80) | 0x61, a & 0x7f, b))

if __name__ == '__main__':
    for i in range(256):
        for j in range(256):
            for k in range(256):
                a, b = unpack3(i, j, k)
                assert pack(a, b) == bytes((0xc4, i, j, k)), '%02x %02x %02x'%(i, j, k)
    for i in range(256):
        for j in range(256):
            a, b = unpack2(i, j)
            assert pack(a, b) == c2to3(i, j), '%02x %02x'%(i, j)
