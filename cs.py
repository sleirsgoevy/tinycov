import sys, capstone

cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
print(next(cs.disasm(bytes.fromhex(''.join(sys.argv[1:])), 0)))
