import capstone

cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
while True:
    try: line = input()
    except EOFError: break
    print(next(cs.disasm(bytes.fromhex(''.join(line.split())), 0)))
