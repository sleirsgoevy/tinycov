import capstone

cs = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)

while True:
    q = bytes.fromhex(''.join(input().split()))
    try: qq = next(cs.disasm(q, 0))
    except StopIteration:
        print(q.hex(), 'invalid')
    else:
        if qq.bytes != q:
            print(qq, 'is shorter than', q.hex())
