{ nixpkgs ? import<nixpkgs>{} }:

with nixpkgs;

stdenvNoCC.mkDerivation {
  name = "tinycov";
  nativeBuildInputs = [ gdb python3Packages.capstone python3Packages.pillow ];
  buildInputs = [ python3Packages.numpy octave ];
}
