#include <stdio.h>
#include <stdlib.h>

void print_maps(void)
{
    char buf[64];
    sprintf(buf, "cat /proc/%d/maps", getpid());
    system(buf);
}

int fib(int a)
{
    if(a < 2)
        return 1;
    return fib(a-1) + fib(a-2);
}

int cmp(const void* a, const void* b)
{
    return *(const char*)a - *(const char*)b;
}

int main()
{
    print_maps();
    setup();
    char* s1 = "abcdefabcdefabcdefabcdefabcdefabcdefabcdef";
    int d = strspn(s1, "a");
    printf("%d\n", d);
    char* s = "%d"; //"                    %d"+20; //"          %d"+10;
    char* ss = strchrnul(s, '%');
    //asm volatile(".byte 0x0f, 0x1f, 0x25, 0x78, 0x56, 0x34, 0x12");
    printf("%p %p\n", s, ss);
    //return 0;
    for(int i = 0; i <= 10; i++)
        printf("%d\n", fib(i));
    int a, b;
    scanf("%d%d", &a, &b);
    printf("%d\n", a+b);
    char buf[32];
    for(int i = 0; i < 100; i++)
        sprintf(buf, "%d %x\n", i, 99-i);
    char buf2[6] = "hello";
    qsort(buf2, 5, 1, cmp);
    printf("%s\n", buf2);
    return 0;
}
