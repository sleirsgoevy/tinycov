#pragma once

/* structures and constants */

struct trap_state
{
    uint64_t regs[18];
};

enum trap_reg
{
    TRAP_REG_RAX,
    TRAP_REG_RCX,
    TRAP_REG_RDX,
    TRAP_REG_RBX,
    TRAP_REG_RSP,
    TRAP_REG_RBP,
    TRAP_REG_RSI,
    TRAP_REG_RDI,
    TRAP_REG_R8,
    TRAP_REG_R9,
    TRAP_REG_R10,
    TRAP_REG_R11,
    TRAP_REG_R12,
    TRAP_REG_R13,
    TRAP_REG_R14,
    TRAP_REG_R15,
    TRAP_REG_RIP,
    TRAP_REG_EFLAGS,
};

enum trap_exc
{
    TRAP_INT1,
};

/* tinycov interface to host */

void tinycov_hal_set_handler(enum trap_exc, void(*)(struct trap_state*));
void* tinycov_hal_map_memory(size_t size);
void* tinycov_hal_get_coverage_shmem(void);
void tinycov_hal_midsetup(void);
int tinycov_hal_enter(void);
void tinycov_hal_exit(int);

/* tinycov debug output (needed only if DEBUG_PRINTS or TRACE are set) */

void tinycov_debug_putchar(char c);
void tinycov_debug_write(const char* s, size_t n);

/* host interface to tinycov */

void tinycov_setup(void);
void* tinycov_entry(void*);
