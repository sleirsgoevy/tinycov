/*
Copyright 2022 Sergey Lisov

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
*/

#ifndef SKIP_HEADERS
#include <stdint.h>
typedef uintptr_t size_t;
typedef uint64_t off_t;
#endif
#include "tinycov-hal.h"
#include "tinycov-config.h"

/* generic kernel HAL. tinycov_hal_map_memory is OS-specific, thus not covered here */

struct idtr
{
    uint16_t size;
    uint64_t pointer;
} __attribute__((packed));

extern char check_idtr_6_bytes[1/(sizeof(struct idtr) == 10)];

uint64_t tinycov_old_int1;
uint64_t tinycov_new_int1;
uint64_t tinycov_old_int3;
uint64_t tinycov_new_int3;
uint64_t tinycov_old_int13;
uint64_t tinycov_old_int14;
extern char tinycov_int1[];
extern char tinycov_int3[];
extern char tinycov_int9[];
extern char tinycov_int13[];
extern char tinycov_int14[];

static void* coverage_shmem;

static struct idtr idtr;
static uint64_t lstar;

static void asm_setup(void)
{
    asm volatile("sidt %0":"=m"(idtr));
    int nentries = (idtr.size + 1) / 16;
    if(nentries > 256)
        nentries = 256;
    uint64_t* old_idt = (void*)idtr.pointer;
    uint64_t* new_idt = tinycov_hal_map_memory(nentries * 16);
    for(int i = 0; i < nentries; i++)
    {
#ifdef LINUX_IDT_WORKAROUND
        if(i >= 20 && i < 32)
        {
            new_idt[2*i] = old_idt[2*i];
            new_idt[2*i+1] = old_idt[2*i+1];
            continue;
        }
#endif
        uint64_t old = (old_idt[2*i+1] << 32) | ((old_idt[2*i] >> 32) & 0xffff0000) | (uint16_t)old_idt[2*i];
        //TODO: call jit_out before calling the handler, to conceal ourselves.
        uint64_t new = (uint64_t)tinycov_entry((void*)old);
        if(i == 1)
        {
            tinycov_old_int1 = new;
            new = (uint64_t)tinycov_int1;
        }
        else if(i == 3)
        {
            tinycov_old_int3 = new;
            new = (uint64_t)tinycov_int3;
        }
        else if(i == 9)
            new = (uint64_t)tinycov_int9;
        else if(i == 13)
        {
            tinycov_old_int13 = new;
            new = (uint64_t)tinycov_int13;
        }
        else if(i == 14)
        {
            tinycov_old_int14 = new;
            new = (uint64_t)tinycov_int14;
        }
        new_idt[2*i] = (old_idt[2*i] & 0xffffffff0000) | (uint16_t)new | ((new & 0xffff0000) << 32);
        new_idt[2*i+1] = new >> 32;
        if(i == 9)
            new_idt[2*i] |= 0x600000000000; //allow access from userspace
        if(i == 1)
            new_idt[2*i] &= ~0x700000000; //prevent it from using interrupt stack
    }
    idtr.pointer = (uint64_t)new_idt;
    uint32_t d, a, c = 0xc0000082;
    asm volatile("rdmsr":"=d"(d),"=a"(a):"c"(c));
    lstar = (uint64_t)d << 32 | a;
    lstar = (uint64_t)tinycov_entry((void*)lstar);
}

asm(
".section .text\n"
"tinycov_int1:\n"
"testb $3, 8(%rsp)\n"
"je tinycov_int1_kernel\n"
"jmp *tinycov_old_int1(%rip)\n"
"tinycov_int3:\n"
"testb $3, 8(%rsp)\n"
"je tinycov_int1_kernel\n"
"jmp *tinycov_old_int3(%rip)\n"
"tinycov_int1_kernel:\n"
"pushq 16(%rsp)\n"
"pushq 8(%rsp)\n"
"push %r15\n"
"push %r14\n"
"push %r13\n"
"push %r12\n"
"push %r11\n"
"push %r10\n"
"push %r9\n"
"push %r8\n"
"push %rdi\n"
"push %rsi\n"
"push %rbp\n"
"pushq 0x80(%rsp)\n"
"push %rbx\n"
"push %rdx\n"
"push %rcx\n"
"push %rax\n"
"mov %rsp, %rdi\n"
"push %rbp\n"
"mov %rsp, %rbp\n"
"and $-16, %rsp\n"
"cld\n"
"call *tinycov_new_int1(%rip)\n"
"leave\n"
"pop %rax\n"
"pop %rcx\n"
"pop %rdx\n"
"pop %rbx\n"
"popq 0x80(%rsp)\n"
"pop %rbp\n"
"pop %rsi\n"
"pop %rdi\n"
"pop %r8\n"
"pop %r9\n"
"pop %r10\n"
"pop %r11\n"
"pop %r12\n"
"pop %r13\n"
"pop %r14\n"
"pop %r15\n"
"popq 8(%rsp)\n"
"popq 16(%rsp)\n"
"iretq\n"
);

int tinycov_copyio(void*, const void*, size_t);

uint64_t tinycov_ctl(uint64_t rdi, uint64_t rsi)
{
    char* dst = 0;
    char* src = 0;
    size_t size = 0;
    if(rdi == 1234)
    {
        dst = (void*)rsi;
        src = coverage_shmem;
        size = COVERAGE_SIZE;
    }
    else if(rdi == 4321)
    {
        dst = coverage_shmem;
        src = (void*)rdi;
        size = COVERAGE_SIZE;
    }
    else if(rdi == 5678)
    {
        asm volatile("lidt %0"::"m"(idtr));
        asm volatile("wrmsr"::"a"((uint32_t)lstar),"d"((uint32_t)(lstar >> 32)),"c"(0xc0000082));
    }
    else
        return -1;
    return tinycov_copyio(dst, src, size);
}

/* direct interface to tinycov_ctl, bypassing the kernel */
asm(
".section .text\n"
"tinycov_int9:\n"
"push %rbp\n"
"mov %rsp, %rbp\n"
"and $-16, %rsp\n"
"call tinycov_ctl\n"
"leave\n"
"iretq\n"
);

asm(
".section .text\n"
"tinycov_copyio:\n"
"mov %rdx, %rcx\n"
"tinycov_copyio_main:\n"
"rep movsb\n"
"xor %eax, %eax\n"
"ret\n"
"tinycov_copyio_fail:\n"
"xor %eax, %eax\n"
"dec %eax\n"
"ret\n"
);

asm(
".section .text\n"
"tinycov_int13:\n"
"jmp *tinycov_old_int13(%rip)\n"
);

asm(
".section .text\n"
"tinycov_int14:\n"
"push %rax\n"
"lea tinycov_copyio_main(%rip), %rax\n"
"cmp 16(%rsp), %rax\n"
"je tinycov_int14_fake\n"
"pop %rax\n"
"jmp *tinycov_old_int14(%rip)\n"
"tinycov_int14_fake:\n"
"pop %rax\n"
"addq $(tinycov_copyio_fail-tinycov_copyio_main), 8(%rsp)\n"
"iretq\n"
);

void tinycov_hal_set_handler(enum trap_exc which, void(*fn)(struct trap_state*))
{
    if(which == TRAP_INT1)
        tinycov_new_int1 = (uint64_t)fn;
}

void* tinycov_hal_get_coverage_shmem(void)
{
    if(!coverage_shmem)
        coverage_shmem = tinycov_hal_map_memory(COVERAGE_SIZE);
    return coverage_shmem;
}

void tinycov_hal_midsetup(void)
{
    asm_setup();
    /* userspace should take care of entering trace mode */
}

#if defined(DEBUG_PRINTS) || defined(TRACE)

//use bochs' paravirtualized debug port

void tinycov_debug_putchar(char c)
{
    asm volatile("out %%al, $0xe9"::"a"(c));
}

void tinycov_debug_write(const char* s, size_t n)
{
    while(n--)
        tinycov_debug_putchar(*s++);
}

#endif

asm(
".section .text\n"
".global tinycov_hal_enter\n"
"tinycov_hal_enter:\n"
"pushfq\n"
"pop %rax\n"
"shr $9, %rax\n"
"and $1, %rax\n"
"cli\n"
"ret\n"
);

void tinycov_hal_exit(int q)
{
    if(q)
        asm volatile("sti");
}
