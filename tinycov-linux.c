/*
Copyright 2022 Sergey Lisov

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <ucontext.h>
#include "tinycov-hal.h"
#include "tinycov-config.h"

static void* coverage;

void* tinycov_hal_get_coverage_shmem(void)
{
    return coverage;
}

void* tinycov_hal_map_memory(size_t size)
{
    return mmap(0, size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
}

void tinycov_hal_midsetup(void)
{
#ifdef AFL_FORKSERVER
    uint32_t x = 0;
    write(199, &x, 4);
    while(read(198, &x, 4) == 4)
    {
        x = fork();
        if(!x)
            goto child;
        write(199, &x, 4);
        int status;
        waitpid(x, &status, WUNTRACED);
        x = status;
        write(199, &x, 4);
    }
    close(198);
    close(199);
child:;
#endif
}

#if defined(DEBUG_PRINTS) || defined(TRACE)
void tinycov_debug_write(const char* s, size_t n)
{
    write(2, s, n);
}

void tinycov_debug_putchar(char c)
{
    tinycov_debug_write(&c, 1);
}
#endif

#ifdef SETSID
int setsid(void)
#else
#ifndef TEST_MODE
__attribute__((constructor)) static
#endif
void setup(void)
#endif
{
#ifdef AFL
    char* ge = getenv("__AFL_SHM_ID");
    if(ge)
        coverage = shmat(atoi(ge), NULL, 0);
    else
#endif
        coverage = mmap(0, COVERAGE_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
    tinycov_setup();
    asm volatile("pop %%rcx\npushfq\norb $1, 1(%%rsp)\npopfq\npush %%rcx":::"rcx");
#ifdef SETSID
    return 0;
#endif
}

#ifdef TEST_MODE

#if defined(DEBUG_PRINTS) || defined(TRACE)
#undef printf
#undef sprintf
#undef snprintf
#undef vsnprintf
#undef vprintf
#endif

#include <stdio.h>

extern int hits[256];
extern uint64_t last_hit[256];
extern int hits2[256];
extern uint64_t last_hit2[256];

__attribute__((destructor))
static void stats(void)
{
    asm volatile(".byte 0x0f, 0x1f, 0x25, 0x78, 0x56, 0x34, 0x12");
    signal(SIGTRAP, SIG_DFL);
    for(int i = 0; i < 256; i++)
        printf("%d %02x (last 0x%016llx)\n", hits[i], i, last_hit[i]);
    for(int i = 0; i < 256; i++)
        printf("%d 0f%02x (last 0x%016llx)\n", hits2[i], i, last_hit2[i]);
}
#endif

static void(*int1_handler)(struct trap_state*);

static void sigtrap_handler(int sig, siginfo_t* si, void* o_uc)
{
    ucontext_t* uc = o_uc;
    struct trap_state ts;
    ts.regs[TRAP_REG_RAX] = uc->uc_mcontext.gregs[REG_RAX];
    ts.regs[TRAP_REG_RCX] = uc->uc_mcontext.gregs[REG_RCX];
    ts.regs[TRAP_REG_RDX] = uc->uc_mcontext.gregs[REG_RDX];
    ts.regs[TRAP_REG_RBX] = uc->uc_mcontext.gregs[REG_RBX];
    ts.regs[TRAP_REG_RSP] = uc->uc_mcontext.gregs[REG_RSP];
    ts.regs[TRAP_REG_RBP] = uc->uc_mcontext.gregs[REG_RBP];
    ts.regs[TRAP_REG_RSI] = uc->uc_mcontext.gregs[REG_RSI];
    ts.regs[TRAP_REG_RDI] = uc->uc_mcontext.gregs[REG_RDI];
    ts.regs[TRAP_REG_R8] = uc->uc_mcontext.gregs[REG_R8];
    ts.regs[TRAP_REG_R9] = uc->uc_mcontext.gregs[REG_R9];
    ts.regs[TRAP_REG_R10] = uc->uc_mcontext.gregs[REG_R10];
    ts.regs[TRAP_REG_R11] = uc->uc_mcontext.gregs[REG_R11];
    ts.regs[TRAP_REG_R12] = uc->uc_mcontext.gregs[REG_R12];
    ts.regs[TRAP_REG_R13] = uc->uc_mcontext.gregs[REG_R13];
    ts.regs[TRAP_REG_R14] = uc->uc_mcontext.gregs[REG_R14];
    ts.regs[TRAP_REG_R15] = uc->uc_mcontext.gregs[REG_R15];
    ts.regs[TRAP_REG_RIP] = uc->uc_mcontext.gregs[REG_RIP];
    ts.regs[TRAP_REG_EFLAGS] = uc->uc_mcontext.gregs[REG_EFL];
    int1_handler(&ts);
    uc->uc_mcontext.gregs[REG_RAX] = ts.regs[TRAP_REG_RAX];
    uc->uc_mcontext.gregs[REG_RCX] = ts.regs[TRAP_REG_RCX];
    uc->uc_mcontext.gregs[REG_RDX] = ts.regs[TRAP_REG_RDX];
    uc->uc_mcontext.gregs[REG_RBX] = ts.regs[TRAP_REG_RBX];
    uc->uc_mcontext.gregs[REG_RSP] = ts.regs[TRAP_REG_RSP];
    uc->uc_mcontext.gregs[REG_RBP] = ts.regs[TRAP_REG_RBP];
    uc->uc_mcontext.gregs[REG_RSI] = ts.regs[TRAP_REG_RSI];
    uc->uc_mcontext.gregs[REG_RDI] = ts.regs[TRAP_REG_RDI];
    uc->uc_mcontext.gregs[REG_R8] = ts.regs[TRAP_REG_R8];
    uc->uc_mcontext.gregs[REG_R9] = ts.regs[TRAP_REG_R9];
    uc->uc_mcontext.gregs[REG_R10] = ts.regs[TRAP_REG_R10];
    uc->uc_mcontext.gregs[REG_R11] = ts.regs[TRAP_REG_R11];
    uc->uc_mcontext.gregs[REG_R12] = ts.regs[TRAP_REG_R12];
    uc->uc_mcontext.gregs[REG_R13] = ts.regs[TRAP_REG_R13];
    uc->uc_mcontext.gregs[REG_R14] = ts.regs[TRAP_REG_R14];
    uc->uc_mcontext.gregs[REG_R15] = ts.regs[TRAP_REG_R15];
    uc->uc_mcontext.gregs[REG_RIP] = ts.regs[TRAP_REG_RIP];
    uc->uc_mcontext.gregs[REG_EFL] = ts.regs[TRAP_REG_EFLAGS];
}

void tinycov_hal_set_handler(enum trap_exc exc, void(*fn)(struct trap_state*))
{
    if(exc == TRAP_INT1)
    {
        int1_handler = fn;
        struct sigaction sa = {
            .sa_sigaction = sigtrap_handler,
            .sa_flags = SA_SIGINFO,
        };
        sigaction(SIGTRAP, &sa, NULL);
    }
}

int tinycov_hal_enter(void)
{
    return 0;
}

void tinycov_hal_exit(int _){}
