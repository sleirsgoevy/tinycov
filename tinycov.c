/*
Copyright 2022 Sergey Lisov

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
*/

#ifndef SKIP_HEADERS
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <sys/types.h>
#ifdef LINUX_SYSCALL_HOOK
#include <signal.h>
#include <sys/syscall.h>
#endif
#endif
#include "tinycov-hal.h"
#include "tinycov-config.h"

extern char protect_start[];
extern char protect_end[];

asm(".section .text\nprotect_start:\n");

/* need to provide our own implementations to avoid trashing fpu/sse registers */

static inline void* memcpy(void* dst, const void* src, size_t sz)
{
    char* dstc = dst;
    const char* srcc = src;
    while(sz--)
        *dstc++ = *srcc++;
    return dst;
}

static inline void* memset(void* dst, int c, size_t n)
{
    char* s = dst;
    while(n--)
        *s++ = c;
    return dst;
}

static inline int memcmp(const void* a, const void* b, size_t sz)
{
    const char* s1 = a;
    const char* s2 = b;
    while(sz && *s1 == *s2)
        s1++, s2++, sz--;
    return sz ? (*s1 - *s2) : 0;
}

#if defined(DEBUG_AFTER) || defined(MAX_COUNT)
static int counter;
#endif

#if defined(DEBUG_PRINTS) || defined(TRACE)

void _putchar(char c)
{
    tinycov_debug_putchar(c);
}

#define PRINTF_DISABLE_SUPPORT_FLOAT
#include "printf/printf.c"
#endif

#ifdef DEBUG_PRINTS

#ifdef DEBUG_AFTER
static inline int DEBUG(const char* fmt, ...)
{
    if(counter < DEBUG_AFTER)
        return 0;
    va_list args;
    va_start(args, fmt);
    return vprintf(fmt, args);
}
#else
#define DEBUG printf
#endif

#else
#define DEBUG(...)
#endif

/* chosen with a fair dice roll */
#define COVERAGE_XOR 0xb5497ed1759251f8
#define COVERAGE_CMOV_OK 0xe0758132583c1069 
#define COVERAGE_CMOV_FAIL 0xeb6c8444c299dfb4

static uint8_t* jit_memory;
static uint8_t* jit_shadow;
static uint8_t* coverage;
static struct
{
    size_t jit_alloc_offset;
    void* jit_root_table;
}* p_jit_state;
#define jit_alloc_offset (p_jit_state->jit_alloc_offset)
#define jit_root_table (p_jit_state->jit_root_table)

static void* alloc_aligned(size_t size, size_t align)
{
    size_t old_offset = __atomic_load_n(&jit_alloc_offset, __ATOMIC_SEQ_CST);
    size_t ans = 0;
    do
        ans = ((old_offset - 1) | (align - 1)) + size + 1;
    while(!__atomic_compare_exchange_n(&jit_alloc_offset, &old_offset, ans, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST));
    DEBUG("alloc_aligned: %d bytes used\n", ans);
    return (void*)(jit_memory + (ans - size));
}

static void* get_translated(uint64_t rip, int wait)
{
    void** table_ptr = &jit_root_table;
    void* table_alloc = NULL;
    for(int i = 5; i >= 0; i--)
    {
        void* ptr = __atomic_load_n(table_ptr, __ATOMIC_SEQ_CST);
        if(!ptr)
        {
            if(!table_alloc)
                table_alloc = alloc_aligned(sizeof(void*)*256, sizeof(void*));
            if(__atomic_compare_exchange_n(table_ptr, &ptr, table_alloc, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST))
            {
                ptr = table_alloc;
                table_alloc = NULL;
            }
        }
        uint8_t byte = (rip >> (i * 8));
        table_ptr = (void**)ptr + byte;
    }
    if(!wait)
        return table_ptr;
    void* ans = __atomic_load_n(table_ptr, __ATOMIC_SEQ_CST);
    if((uintptr_t)ans > 1)
        return ans;
    if(!ans)
    {
        DEBUG("get_translated(0x%016llx) tries to grab the lock\n", rip);
        __atomic_compare_exchange_n(table_ptr, &ans, (void*)1, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
    }
    while((uintptr_t)ans == 1)
    {
        DEBUG("get_translated(0x%016llx) waiting for other thread\n", rip);
        ans = __atomic_load_n(table_ptr, __ATOMIC_SEQ_CST);
    }
    return ans;
}

static void* set_translated(uint64_t rip, void* translated)
{
    DEBUG("set_translated(0x%016llx, %p)\n", rip, translated);
    void** table_ptr = get_translated(rip, 0);
    __atomic_store_n(table_ptr, translated, __ATOMIC_SEQ_CST);
    return table_ptr;
}

/* shadow memory bytes:
00 = nop
01 = original 1 byte, translated 1 byte
02-0x7f = translated %d bytes, original size follows
0x80-0x8f = original %d bytes, translated is behind
0x90 = add rsp, 128
0x91 = cc trap, unknown instruction
0x92 = popfq
0x93 = pop the corresponding register
0x95 = pop qword and discard it
0xc0-0xcf original %d bytes, translated is behind, also skip the next instruction
0xd0-0xdf original %d bytes, translated 1 byte
0xe0-0xef skip %d nonzero markers
0xfd = noreturn point before jump. parse a 5-byte jump after this and jump to it
0xfe = beginning of dynamic chunk, continue from the specified address
0xff = beginning of chunk, original rip stored 8 bytes before
*/
static void jit_out(struct trap_state* uc)
{
    uint64_t rip = uc->regs[TRAP_REG_RIP];
    if(rip >= (uint64_t)jit_memory && rip < (uint64_t)(jit_memory + JIT_MEMORY_SIZE))
    {
        uint8_t* shadow_ptr = jit_shadow + ((uint8_t*)rip - jit_memory);
        shadow_ptr--;
        if(*shadow_ptr == 0x91)
            rip = *(uint64_t*)rip;
        else
        {
            uint64_t rip_offset = 0;
            int skip = 0;
            while(*shadow_ptr != 0xff)
            {
                if(*shadow_ptr == 0xfe) //make it higher priority than skips
                {
                    shadow_ptr = jit_shadow + (*(uint8_t**)(shadow_ptr-8) - jit_memory) - 1;
                    continue;
                }
                if(skip)
                {
                    if(*shadow_ptr)
                        skip--;
                    shadow_ptr--;
                    continue;
                }
                if(*shadow_ptr >= 0x80 && *shadow_ptr < 0x90)
                    rip_offset += *shadow_ptr - 0x80;
                else if(*shadow_ptr == 1)
                    rip_offset++;
                else if(*shadow_ptr == 0x90)
                    uc->regs[TRAP_REG_RSP] += 128;
                else if(*shadow_ptr == 0x92)
                {
                    uc->regs[TRAP_REG_EFLAGS] = *(uint64_t*)uc->regs[TRAP_REG_RSP];
                    uc->regs[TRAP_REG_RSP] += 8;
                }
                else if(*shadow_ptr == 0x93)
                {
                    uint64_t value = *(uint64_t*)uc->regs[TRAP_REG_RSP];
                    uc->regs[TRAP_REG_RSP] += 8;
                    uint8_t* rip_ptr = jit_memory + (shadow_ptr - jit_shadow);
                    int reg;
                    if(*rip_ptr == 0x49)
                        reg = rip_ptr[1] - 0x50 + 8;
                    else
                        reg = rip_ptr[0] - 0x50;
                    uc->regs[reg] = value;
                }
                else if(*shadow_ptr == 0x95)
                    uc->regs[TRAP_REG_RSP] += 8;
                else if(*shadow_ptr >= 0xc0 && *shadow_ptr < 0xd0)
                {
                    rip_offset += *shadow_ptr - 0xc0;
                    skip = 1;
                }
                else if(*shadow_ptr >= 0xd0 && *shadow_ptr < 0xe0)
                    rip_offset += *shadow_ptr - 0xd0;
                else if(*shadow_ptr >= 0xe0 && *shadow_ptr < 0xf0)
                    skip = *shadow_ptr - 0xe0;
                else if(*shadow_ptr == 0xfd)
                {
                    uint8_t* rip_ptr = jit_memory + (shadow_ptr - jit_shadow);
                    int32_t diff = *(int32_t*)(rip_ptr + 2);
                    uint8_t* dst = rip_ptr + 6 + diff;
                    shadow_ptr = jit_shadow + (dst - jit_memory);
                }
                shadow_ptr--;
            }
            rip = *(uint64_t*)(shadow_ptr - 8) + rip_offset;
        }
    }
    else if((uc->regs[TRAP_REG_EFLAGS] & 256))
        uc->regs[TRAP_REG_EFLAGS] -= 256;
    uc->regs[TRAP_REG_RIP] = rip;
}

typedef void feed_t(void* opaque, const uint8_t* src1, const uint8_t* src2, size_t n, size_t m);

static void feed_increment(void* opaque, const uint8_t* src1, const uint8_t* src2, size_t n, size_t m)
{
    *(size_t*)opaque += n;
}

static void out2(void* opaque, const uint8_t* src1, const uint8_t* src2, size_t n, size_t m)
{
    if(m > n)
        m = n;
    void** dst = opaque;
    uint8_t* dst1 = *dst;
    uint8_t* dst2 = jit_shadow + (dst1 - jit_memory);
    memcpy(dst1, src1, n);
    memcpy(dst2, src2, m);
    memset(dst2+m, 0, n-m);
    *dst = dst1 + n;
}

static uint32_t branch_hash(uint64_t a, uint64_t b)
{
    a = (a ^ COVERAGE_XOR) % COVERAGE_MOD;
    b = (b ^ COVERAGE_XOR) % COVERAGE_MOD;
    return ((a >> 1) ^ b) % COVERAGE_SIZE;
}

static uint64_t parse_jump(uint64_t rip, int* cc, uint64_t* dst)
{
    uint8_t* p = (uint8_t*)rip;
    if(*p == 0xe9 /* jump */ || *p == 0xe8 /* call */)
    {
        *cc = 0xf9 - *p;
        *dst = rip + 5 + *(int32_t*)(p+1);
        return rip + 5;
    }
    else if(*p == 0xeb)
    {
        *cc = 16;
        *dst = rip + 2 + (int8_t)p[1];
        return rip + 2;
    }
    return 0;
}

static uint64_t parse_condjump(uint64_t rip, int* cc, uint64_t* dst)
{
    uint8_t* p = (uint8_t*)rip;
    if(*p >= 0x70 && *p < 0x80)
    {
        *cc = *p - 0x70;
        *dst = rip + 2 + (int8_t)p[1];
        return rip + 2;
    }
    else if(*p == 0x0f && (p[1] >= 0x80 && p[1] < 0x90))
    {
        *cc = p[1] - 0x80;
        *dst = rip + 6 + *(int32_t*)(p+2);
        return rip + 6;
    }
    return parse_jump(rip, cc, dst);
}

static void* compile(uint64_t rip, int stub_on_fail);

void lazy_condjump(uint64_t* p_rip)
{
    int enter = tinycov_hal_enter();
    DEBUG("in lazy_condjump!\n");
    uint64_t rip = *p_rip;
    struct trap_state fake_ctx = {0};
    fake_ctx.regs[TRAP_REG_RSP] = 0; //trying to pop anything is a fatal error
    fake_ctx.regs[TRAP_REG_RIP] = rip;
    jit_out(&fake_ctx);
    uint64_t rip0 = fake_ctx.regs[TRAP_REG_RIP];
    int cc;
    uint64_t dst0;
    if(!parse_condjump(rip0, &cc, &dst0))
        asm volatile("ud2");
    DEBUG("dst0 = 0x%016llx\n", dst0);
    void* dst_p = get_translated(dst0, 1);
    if(!dst_p || dst_p == (void*)2 /* hack: overwrite already saved stub */)
    {
        dst_p = compile(dst0, 1);
        set_translated(dst0, dst_p);
    }
    DEBUG("-> %p\n", dst_p);
    uint64_t dst = (uint64_t)dst_p;
    rip -= 6;
    *p_rip = rip;
    uint8_t* p = (uint8_t*)rip;
    //XXX: what if someone is currently running the code we're gonna patch?
    if(cc >= 16) //unconditional
    {
        memcpy(p-5, "\xe9\x00\x00\x00\x00\x48\x8d\xa4\x24\x80\x00\x00\x00\xeb\xf1", 15);
        *(int32_t*)(p - 4) = dst - rip;
    }
    else
    {
        memcpy(p, "\x9d\x48\x8d\xa4\x24\x80\x00\x00\x00\xe9", 10);
        *(int32_t*)(p + 10) = dst - (rip + 14);
    }
    tinycov_hal_exit(enter);
}

#define PUSH_ALL \
"push %rdi\n"\
"lea 8(%rsp), %rdi\n"\
"push %rax\n"\
"push %rcx\n"\
"push %rdx\n"\
"push %rsi\n"\
"push %r8\n"\
"push %r9\n"\
"push %r10\n"\
"push %r11\n"\
"pushfq\n"\
"push %rbp\n"\
"mov %rsp, %rbp\n"\
"and $0xfffffffffffffff0, %rsp\n"\
"cld\n"

#define POP_ALL \
"leave\n"\
"popfq\n"\
"pop %r11\n"\
"pop %r10\n"\
"pop %r9\n"\
"pop %r8\n"\
"pop %rsi\n"\
"pop %rdx\n"\
"pop %rcx\n"\
"pop %rax\n"\
"pop %rdi\n"

asm(
".section .text\n"
"asm_lazy_condjump:\n"
PUSH_ALL
"call lazy_condjump\n"
POP_ALL
"ret\n"
);

extern char asm_lazy_condjump[];

static void handle_condjump(feed_t* feed, void* opaque, int cc, uint64_t from, uint64_t to, size_t orig_sz)
{
    //to patch total size
    uint8_t* very_start;
    uint8_t* very_start_shadow;
    if(feed == out2)
    {
        very_start = *(void**)opaque;
        very_start_shadow = jit_shadow + (very_start - jit_memory);
    }
    feed(opaque, "\x48\x8d\x64\x24\x80", "\x00\x90", 5, 2); //lea rsp, [rsp-128]
    feed(opaque, "\x9c", "\x92", 1, 1); //pushfq
    feed(opaque, "\x50", "\x93", 1, 1); //push rax
    feed(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
    feed(opaque, (uint8_t*)&coverage, NULL, 8, 0); //...coverage
    uint8_t* jump;
    if(feed == out2)
        jump = *(void**)opaque;
    feed(opaque, "\0\0", NULL, 2, 0); //inverted condjump forward
    /* positive option. increment coverage and jump away */
    uint32_t hash1 = branch_hash(from, to);
    feed(opaque, "\xf0\xfe\x80", NULL, 3, 0); //lock inc [rax+...
    feed(opaque, (uint8_t*)&hash1, NULL, 4, 0); //...hash1], TODO: decrement if canceled
    feed(opaque, "\x58", "\xe1", 1, 1); //pop rax
    uint8_t* resolve;
    if(feed == out2)
        resolve = *(void**)opaque;
    feed(opaque, "\x9d", "\xe3", 1, 1); //popfq
    feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", "\xe5", 8, 1); //lea rsp, [rsp+128]
    feed(opaque, "\xe9\x00\x00\x00\x00", "\xe3", 5, 1); //jmp yet_unknown
    uint8_t* jump_target;
    if(feed == out2)
        jump_target = *(void**)opaque;
    /* negative option. increment coverage and keep running */
    uint32_t hash2 = branch_hash(from, from+orig_sz);
    feed(opaque, "\xf0\xfe\x80", NULL, 3, 0); //lock inc [rax+...
    feed(opaque, (uint8_t*)&hash2, NULL, 4, 0); //...hash2], TODO: decrement if canceled
    feed(opaque, "\x58", "\xe5", 1, 1); //pop rax
    feed(opaque, "\x9d", "\xe7", 1, 1); //popfq
    uint8_t shadow[2] = {0xe9, 0x80+orig_sz};
    feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", shadow, 8, 2); //lea rsp, [rsp+128]
    /* patch yet-undefined fields in */
    if(feed == out2)
    {
        //jump
        jump[0] = 0x70 + (cc ^ 1);
        jump[1] = jump_target - (jump + 2);
        //resolve
        resolve[0] = 0xff;
        resolve[1] = 0x15;
        *(uint32_t*)(resolve+2) = 0;
        *(uint64_t*)(resolve+6) = (uint64_t)asm_lazy_condjump;
        //total size
        *very_start_shadow = (uint8_t*)*(void**)opaque - very_start;
    }
}

static void push_lr(feed_t* feed, void* opaque, uint64_t lr)
{
    feed(opaque, "\x50", "\x93", 1, 1); //push rax
    feed(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
    feed(opaque, (uint8_t*)&lr, NULL, 8, 0); //...from
    feed(opaque, "\x48\x87\x04\x24", "\xe1\x95", 4, 2); //xchg rax, [rsp]
}

static void handle_jump(feed_t* feed, void* opaque, int cc, uint64_t from, uint64_t to)
{
    if(cc == 17) // call
        push_lr(feed, opaque, from);
    feed(opaque, "\x48\x8d\x64\x24\x80", "\x90", 5, 1); //lea rsp, [rsp-128]
    feed(opaque, "\xff\x15\x00\x00\x00\x00", "\xe1", 6, 1); //call [rip]
    uint64_t q = (uint64_t)asm_lazy_condjump;
    feed(opaque, (uint8_t*)&q, NULL, 8, 0);
}

#ifdef LINUX_SYSCALL_HOOK

asm(
".section .text\n"
"asm_syscall:\n"
"mov %rcx, %r10\n"
"mov 8(%rsp), %rax\n"
"syscall\n"
"ret\n"
);

uint64_t asm_syscall(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t);

void syscall_hook(uint64_t* args)
{
    int enter = tinycov_hal_enter();
    if(args[0] == __NR_vfork || args[0] == __NR_clone)
    {
        args[8] += 2;
        if(args[2])
            args[2] -= 128;
        tinycov_hal_exit(enter);
        return;
    }
    uint64_t arg2 = args[2];
    if(args[0] == __NR_rt_sigaction && args[1] == SIGTRAP)
        arg2 = 0;
    uint64_t sss = 1;
    if(args[0] == __NR_rt_sigprocmask && args[1] != SIG_UNBLOCK && arg2)
        sss = args[4];
    uint8_t ss[sss];
    if(args[0] == __NR_rt_sigprocmask && args[1] != SIG_UNBLOCK && arg2)
    {
        memcpy(ss, (void*)arg2, sss);
        ss[(SIGTRAP - 1) / 8] &= ~(1 << ((SIGTRAP - 1) % 8));
        arg2 = (uint64_t)ss;
    }
    args[0] = asm_syscall(args[1], arg2, args[3], args[4], args[5], args[6], args[0]);
    tinycov_hal_exit(enter);
}

asm(
".section .text\n"
"asm_syscall_hook:\n"
"pushfq\n"
"push %r9\n"
"push %r8\n"
"push %r10\n"
"push %rdx\n"
"push %rsi\n"
"push %rdi\n"
"push %rax\n"
"mov %rsp, %rdi\n"
"push %rbp\n"
"mov %rsp, %rbp\n"
"and $0xfffffffffffffff0, %rsp\n"
"call syscall_hook\n"
"leave\n"
"pop %rax\n"
"pop %rdi\n"
"pop %rsi\n"
"pop %rdx\n"
"pop %r10\n"
"pop %r8\n"
"pop %r9\n"
"popfq\n"
"ret\n"
);

extern char asm_syscall_hook[];

static uint64_t handle_syscall(feed_t* feed, void* opaque, uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    if(p[0] == 0x0f && p[1] == 0x05)
    {
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x18\x90", 5, 2); //lea rsp, [rsp-128]
        feed(opaque, "\xff\x15\x06\x00\x00\x00", "\x82", 6, 1); //call [rip+6]
        feed(opaque, "\xeb\x0c", "\xe1", 2, 1); //jmp rip+12
        feed(opaque, "\x0f\x05", "\x82", 2, 1); //syscall
        feed(opaque, "\xeb\x08", NULL, 2, 0); //jmp rip+8
        uint64_t ptr = (uint64_t)&asm_syscall_hook;
        feed(opaque, (uint8_t*)&ptr, NULL, 8, 0);
        feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", "\xe4\x82", 8, 2); //lea rsp, [rsp+128]
        return rip + 2;
    }
    return 0;
}

#else

static uint64_t handle_syscall(feed_t* feed, void* opaque, uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    if(p[0] == 0x0f && p[1] == 0x05)
    {
        feed(opaque, p, "\x02\x82", 2, 2);
        return rip + 2;
    }
    return 0;
}

#endif

//stack at p_rip:
//for jmp: [rip] flags rip0 rax [red zone]
//for ret: [rip] flags rax rip0
void dispatch_indirect(uint64_t* p_rip, int ret)
{
    int enter = tinycov_hal_enter();
    DEBUG("in dispatch_indirect!\n");
    uint8_t* p = (uint8_t*)*p_rip;
    uint64_t dispatch_head = *(uint64_t*)p;
    //need to get the source rip so that we can calculate the hash
    struct trap_state fake_ctx = {0};
    uint64_t fake_stack[20] = {0};
    fake_ctx.regs[TRAP_REG_RSP] = (uint64_t)fake_stack;
    fake_ctx.regs[TRAP_REG_RIP] = (uint64_t)p;
    jit_out(&fake_ctx);
    uint64_t rip0 = fake_ctx.regs[TRAP_REG_RIP];
    uint64_t dst0 = p_rip[ret ? 3 : 2];
    DEBUG("rip = 0x%016llx\n", dst0);
    DEBUG("from = 0x%016llx\n", rip0);
    void* dst_p = get_translated(dst0, 1);
    if(!dst_p || dst_p == (void*)2 /* hack: overwrite already saved stub */)
    {
        dst_p = compile(dst0, 1);
        set_translated(dst0, dst_p);
    }
    DEBUG("-> %p\n", dst_p);
    size_t size = ret ? 66 : 70;
    if(dispatch_head >= (uint64_t)jit_memory && dispatch_head < (uint64_t)(jit_memory + JIT_MEMORY_SIZE))
        size -= 10;
    //create a cache entry and fill it with code
    void* dispatch_entry = alloc_aligned(size, 1);
    DEBUG("dispatch_entry = %p\n", dispatch_entry);
    void* opaque = &dispatch_entry;
    out2(opaque, (uint8_t*)&p, (uint8_t*)&p, 8, 8);
    out2(opaque, "\x00", "\xfe", 1, 1);
    void* jump_target = dispatch_entry;
    DEBUG("jump_target = %p\n", jump_target);
    out2(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
    out2(opaque, (uint8_t*)&dst0, NULL, 8, 0); //...dst0
    out2(opaque, "\x48\x39\x44\x24", NULL, 4, 0); //cmp [rsp+...], rax
    if(ret)
        out2(opaque, "\x18", NULL, 1, 0); //...24
    else
        out2(opaque, "\x10", NULL, 1, 0); //...16
    int64_t diff = dispatch_head - ((uint64_t)dispatch_entry + 6);
    if(diff == (int32_t)diff)
    {
        uint8_t instr[6] = {0x0f, 0x85};
        *(int32_t*)(instr + 2) = diff;
        out2(opaque, instr, NULL, 6, 0);
    }
    else if(ret)
        out2(opaque, "\x75\x1a", NULL, 2, 0);
    else
        out2(opaque, "\x75\x1e", NULL, 2, 0);
    uint32_t hash = branch_hash(rip0, dst0);
    uint8_t* hash_addr = coverage + hash;
    out2(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
    out2(opaque, (uint8_t*)&hash_addr, NULL, 8, 0); //...hash_addr
    out2(opaque, "\xf0\xfe\x00", NULL, 3, 0); //lock inc byte [rax]
    out2(opaque, "\x58", "\xe1", 1, 1); //pop rax
    out2(opaque, "\x9d", "\xe3", 1, 1); //popfq
    out2(opaque, "\x58", "\xe5", 1, 1); //pop rax
    if(ret)
        out2(opaque, "\x48\x8d\x64\x24\x08", "\xfd", 5, 1); //lea rsp, [rsp+8]
    else
    {
        out2(opaque, "\x58", "\xe8", 1, 1); //pop rax
        out2(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", "\xfd", 8, 1); //lea rsp, [rsp+128]
    }
    int32_t diff32 = (uint64_t)dst_p - ((uint64_t)dispatch_entry + 5);
    out2(opaque, "\xe9", "\xe4", 1, 0); //jmp ...
    out2(opaque, (uint8_t*)&diff32, NULL, 4, 0); //...dst_p
    if(diff != (int32_t)diff)
    {
        out2(opaque, "\xff\x25\x00\x00\x00\x00", NULL, 6, 0); //jmp [rip]
        out2(opaque, (uint8_t*)&dispatch_head, NULL, 8, 0);
    }
    *(uint64_t*)p = (uint64_t)jump_target;
    *p_rip -= 6;
    tinycov_hal_exit(enter);
}

asm(
".section .text\n"
"asm_dispatch_indirect:\n"
PUSH_ALL
"xor %rsi, %rsi\n"
"call dispatch_indirect\n"
POP_ALL
"ret\n"
);

extern char asm_dispatch_indirect[];

asm(
".section .text\n"
"asm_dispatch_ret:\n"
PUSH_ALL
"mov $1, %rsi\n"
"call dispatch_indirect\n"
POP_ALL
"ret\n"
);

extern char asm_dispatch_ret[];

static uint64_t get_mrm(uint8_t* p_mrm, int rex, uint64_t* mrm_rip, int* mrm_reg, int nnn)
{
    uint8_t mrm = *p_mrm++;
    size_t imm = 0;
    if(mrm >= 0xc0)
        return (uint64_t)p_mrm; //reg-reg
    else if(mrm >= 0x80)
        imm += 4;
    else if(mrm >= 0x40)
        imm++;
    else if((mrm & 7) == 5) //RIP-relative
    {
        if(mrm_rip)
        {
            *mrm_rip = (uint64_t)p_mrm;
            *mrm_reg = nnn ? -1 : ((mrm & 0x38) >> 3);
            return (uint64_t)(p_mrm + 4);
        }
        return 0;
    }
    if((mrm & 7) == 4) //SIB
    {
        uint8_t sib = *p_mrm++;
        if(!(rex & 1) && (sib & 7) == 5 && mrm <= 0x40)
            imm += 4;
    }
    return (uint64_t)(p_mrm + imm);
}

static int skip_prefixes(uint8_t** p_rip)
{
    uint8_t* p = *p_rip;
    int rex = 0;
    int prefixes = 0;
    while((*p == 0x2e) //cs:, only really used by nop
       || (*p >= 0x40 && *p < 0x50)
       || (*p == 0x64) //fs:
       || (*p == 0x65) //gs:
       || (*p == 0x66)
       || (*p == 0xf0)
       || (*p >= 0xf2 && *p < 0xf4))
    {
        if(*p >= 0x40 && *p < 0x50)
            rex = 32 | (*p & 15);
        if(*p == 0x66)
            prefixes |= 16;
        p++;
    }
    rex |= prefixes;
    *p_rip = p;
    return rex;
}

static uint64_t mask_rex(uint8_t* out, uint64_t rip, uint8_t mask, uint8_t mask_add)
{
    uint8_t* p = (uint8_t*)rip;
    while((*p == 0x2e)
       || (*p >= 0x40 && *p < 0x50)
       || (*p == 0x64)
       || (*p == 0x66)
       || (*p == 0xf0)
       || (*p >= 0xf2 && *p < 0xf4))
    {
        if(*p >= 0x40 && *p < 0x50)
            *out = (*p & ~mask) | mask_add;
        else
            *out = *p;
        out++;
        p++;
    }
    return (uint64_t)p;
}

static inline int validate_vex(int a, int b)
{
    if((b == 0x12 || b == 0x16) && a % 8 < 2)
        return 1;
    if((b >= 0x14 && b < 0x16) && a % 4 < 2)
        return 1;
    if((b == 0x2a || b == 0x51 || b == 0x5a) && a % 4 >= 2)
        return 1;
    if((b >= 0x52 && b < 0x54) && a % 4 == 2)
        return 1;
    if(((b >= 0x54 && b < 0x58) || (b == 0xc6)) && a % 4 < 2)
        return 1;
    if((b >= 0x58 && b < 0x5a) || (b >= 0x5c && b < 0x60) || b == 0xc2)
        return 1;
    if(((b >= 0x60 && b < 0x6e) || (b >= 0x74 && b < 0x77) || (b >= 0xd1 && b < 0xd6) || (b >= 0xd8 && b < 0xe6) || (b >= 0xe8 && b < 0xff && b != 0xf0 && b != 0xf7) || (b >= 0x200 && b < 0x20e) || (b >= 0x228 && b < 0x230 && b != 0x22a) || (b >= 0x237 && b < 0x241) || (b >= 0x245 && b < 0x248) || (b >= 0x28c && b < 0x290 && b % 2 == 0) || (b >= 0x290 && b < 0x294) || (b >= 0x296 && b < 0x2c0 && b % 16 >= 6)) && a % 4 == 1)
        return 1;
    if(((b >= 0x7c && b < 0x7e) || (b == 0xd0)) && a % 2 == 1)
        return 1;
    if(((b == 0xc4) || (b >= 0x2dc && b < 0x2e0)) && a % 8 == 1)
        return 1;
    if((b == 0x216 || b == 0x236) && a % 8 == 5)
        return 1;
    if(b == 0x2f2 && a % 8 == 0)
        return 1;
    if(b == 0x2f5 && a % 8 < 4 && a % 8 != 1)
        return 1;
    if(b == 0x2f6 && a % 8 == 3)
        return 1;
    if(b == 0x2f7 && a % 8 < 4)
        return 1;
    if(b % 0x200 < 0x100 && a % 128 < 120)
        return 0;
    if(b >= 0x10 && b < 0x12)
        return 1;
    if(((b == 0x12) || (b >= 0x2c && b < 0x2e)) && a % 4 >= 2)
        return 1;
    if((b == 0x13 || b == 0x17 || (b >= 0x90 && b < 0x92 && a >= 0x200)) && a % 8 < 2)
        return 1;
    if(b == 0x16 && a % 4 == 2)
        return 1;
    if(((b >= 0x28 && b < 0x2a) || (b == 0x2b) || (b >= 0x2e && b < 0x30) || (b == 0x51) || (b == 0x5a)) && a % 4 < 2)
        return 1;
    if(((b >= 0x52 && b < 0x54) || b == 0x77) && a % 4 == 0)
        return 1;
    if(b == 0x5b && a % 4 < 3)
        return 1;
    if(((b == 0x6e) || (b == 0xd6) || (b == 0xe7) || (b == 0x241) || (b == 0x2db)) && a % 8 == 1)
        return 1;
    if((b == 0x6f || b == 0x7f) && (a % 4 == 1 || a % 4 == 2))
        return 1;
    if((b == 0x70 || b == 0xe6) && a % 4 > 0)
        return 1;
    if(b == 0x7e && (a % 8 == 1 || a % 8 == 2))
        return 1;
    if(((b == 0xe7) || (b == 0x216) || (b >= 0x219 && b < 0x21b)) && a % 8 == 5)
        return 1;
    if(b == 0xf0 && a % 4 == 3)
        return 1;
    if((b >= 0x190 && b < 0x192) && a >= 0x200 && a % 128 >= 120 && a % 128 < 122)
        return 0;
    if(((b >= 0x20e && b < 0x210) || (b == 0x213) || (b >= 0x217 && b < 0x219) || (b >= 0x21c && b < 0x226 && b != 0x21f) || (b == 0x22a) || (b >= 0x230 && b < 0x236) || (b >= 0x258 && b < 0x25a) || (b >= 0x278 && b < 0x27a)) && a % 4 == 1)
        return 1;
    if(b == 0x216 && a % 8 == 1)
        return 0;
    if(b % 0x200 >= 0x100)
        return validate_vex(a, b-0x100);
    return 0;
}

static inline int validate_vex_regreg(int a, int b)
{
    int f = validate_vex(a, b);
    if(((b >= 0x41 && b < 0x43) || (b >= 0x45 && b < 0x48)) && a >= 0x200 && (a % 0x100) >= 0xc0 && (a % 8 == 4 || a % 8 == 5))
        return 1;
    if(b == 0x91 && a >= 0x200 && a % 128 >= 120 && a % 8 < 2)
        return 0;
    if(b == 0x90 && a >= 0x200 && a % 256 >= 120 && a % 256 < 122)
        return 0;
    if((b == 0x44 || b == 0x98) && a >= 0x200 && a % 256 >= 248 && a % 256 < 250)
        return 1;
    if(b < 0x200)
    {
        b %= 0x100;
        if((b >= 0x10 && b < 0x12) && a % 4 >= 2)
            return 1;
        if((b == 0x12 || b == 0x16) && a % 8 == 1)
            return 0;
        if(b == 0x4b && a >= 0x200 && (a % 0x100) >= 0xc0 && a % 8 == 5)
            return 1;
        if(a % 128 < 120)
            return f;
        if(b == 0x13 || (b == 0x17 && a % 8 < 2))
            return 0;
        if(b == 0x2b && a % 4 < 2)
            return 0;
        if(b == 0x50 && a % 4 < 2)
            return 1;
        if(b == 0x93 && a % 0x100 >= 0x80 && a % 8 < 2)
            return 1;
        if(b == 0x92 && a >= 0x200 && a % 8 < 2)
            return 1;
        if((b == 0xc5 || b == 0xf7) && a % 8 == 1)
            return 1;
        if(b == 0xd7 && a % 4 == 1)
            return 1;
        if(b == 0xe7 && a % 4 == 1)
            return 0;
        if(b == 0xf0 && a % 4 == 3)
            return 0;
    }
    else
    {
        b %= 0x100;
        if((b >= 0x2c && b < 0x30) && a % 4 == 1)
            return 0;
        if(((b == 0x8c) || (b == 0x8e) || (b >= 0x90 && b < 0x94)) && a % 4 == 1)
            return 0;
        if(a % 128 <= 120)
            return f;
        if(b == 0x1a && a % 8 == 5)
            return 0;
        if(b == 0x2a && a % 4 == 1)
            return 0;
    }
    return f;
}

static uint64_t parse_vex(int rex, int b1, int b2, uint64_t rip, uint64_t* mrm_rip, int* mrm_reg)
{
    if((rex & 32))
        return 0;
    uint8_t* p = (uint8_t*)rip;
    rex |= (~b1 & 128) >> 5;
    if(b1 % 4 == 1)
        rex |= 16;
    int b2a = b2 & ~0x100;
    /* mrm without immediate */
    if((b2a >= 0x10 && b2a < 0x18)
    || (b2a >= 0x28 && b2a < 0x30)
    || (b2a >= 0x51 && b2a < 0x70)
    || (b2a >= 0x74 && b2a < 0x77)
    || (b2a >= 0x7c && b2a < 0x80)
    || (b2a >= 0xd0 && b2a < 0xff && b2 != 0xf7)
    || (b2a >= 0x200 && b2a < 0x210)
    || (b2a == 0x213)
    || (b2a >= 0x216 && b2a < 0x21b)
    || (b2a >= 0x21c && b2a < 0x21f)
    || (b2a >= 0x220 && b2a < 0x226)
    || (b2a >= 0x228 && b2a < 0x242)
    || (b2a >= 0x245 && b2a < 0x248)
    || (b2a >= 0x258 && b2a < 0x25a)
    || (b2a >= 0x278 && b2a < 0x27a)
    || (b2a == 0x28c)
    || (b2a == 0x28e)
    || (b2a >= 0x290 && b2a < 0x294)
    || (b2a >= 0x296 && b2a < 0x2a0)
    || (b2a >= 0x2a6 && b2a < 0x2b0)
    || (b2a >= 0x2b6 && b2a < 0x2c0)
    || (b2a >= 0x2db && b2a < 0x2e0)
    || (b2a == 0x2f2)
    || (b2a >= 0x2f5 && b2a < 0x2f8))
    {
        if(!(*p >= 0xc0 ? validate_vex_regreg(b1, b2) : validate_vex(b1, b2)))
            return 0;
        return get_mrm(p, rex, mrm_rip, mrm_reg, 0);
    }
    /* no payload */
    if(b2 == 0x77 && validate_vex(b1, b2))
        return rip;
    /* mrm with immediate */
    if((b2a == 0x70)
    || (b2a == 0xc2)
    || (b2a == 0xc4)
    || (b2a == 0xc6))
    {
        if(!(*p >= 0xc0 ? validate_vex_regreg(b1, b2) : validate_vex(b1, b2)))
            return 0;
        return get_mrm(p, rex, mrm_rip, mrm_reg, 0)+1;
    }
    /* immediate only */
    if((b2a >= 0x41 && b2a < 0x43)
    || (b2a >= 0x44 && b2a < 0x48)
    || (b2a == 0x4b)
    || (b2a == 0x50)
    || (b2a >= 0x92 && b2a < 0x94)
    || (b2a == 0x98)
    || (b2a == 0xd7)
    || (b2a == 0xf7))
        return rip + 1;
    return 0;
}

static uint64_t get_end(uint64_t rip, uint64_t* mrm_rip, int* mrm_reg)
{
    uint8_t* p = (uint8_t*)rip;
    /* parse prefixes */
    int rex = skip_prefixes(&p);
    /* fetch opcode */
    uint16_t op = *p;
    /* legacy 2-byte instructions */
    if(op == 0x0f)
        op = 0x100 + *++p;
    if(op == 0x138)
        op = 0x200 + *++p;
    if(op == 0x13a)
        op = 0x300 + *++p;
    /* 2-byte rex */
    if(op == 0xc5)
        return parse_vex(rex, (p[1] & 0x80) << 2 | 0x180 | p[1], p[2], (uint64_t)(p + 3), mrm_rip, mrm_reg);
    /* 3-byte rex */
    if(op == 0xc4)
        return parse_vex(rex, (p[1] & 0xe0) << 2 | (p[2] & 0x7f), ((p[1] & 0x1f) - 1) << 9 | (p[2] & 0x80) << 1 | p[3], (uint64_t)(p + 4), mrm_rip, mrm_reg);
    /* mrm with immediate */
    if((op == 0x69 || op == 0x6b)
    || (op >= 0x80 && op < 0x84)
    || (op >= 0xc6 && op < 0xc8)
    || (op >= 0xf6 && op < 0xf8 && !(p[1] & 0x38))
    /* sse */
    || (op == 0x170)
    || (op >= 0x171 && op < 0x173 && !(p[1] & 8) && (p[1] & 0x38))
    || (op == 0x173 && (p[1] & 0x10) == 0x10)
    /* /sse */
    || (op == 0x1ba && (p[1] & 0x38) >= 0x20)
    || (op == 0x1bb)
    /* sse */
    || (op == 0x1c6)
    || (op >= 0x308 && op < 0x310)
    || (op >= 0x362 && op < 0x364))
    {
        size_t opsz = (op == 0x69 || op == 0x81 || op == 0xc7 || op == 0xf7) ? 4 : 1;
        if(opsz != 1 && (rex & 16) && !(rex & 8))
            opsz = 2;
        uint64_t ans = get_mrm(++p, rex, mrm_rip, mrm_reg, 0);
        if(!ans)
            return ans;
        return ans + opsz;
    }
    /* 0xc0, 0xc1 */
    if((op == 0xc0 || op == 0xc1)
    && (p[1] & 0x38) != 0x30)
    {
        uint64_t ans = get_mrm(++p, rex, mrm_rip, mrm_reg, 0);
        if(ans == 0)
            return 0;
        return ans + 1;
    }
    /* mrm without immediate */
    if((op >= 0x84 && op < 0x8f)
    || (op < 0x40 && ((op & 7) < 4))
    || (op == 0x63)
    || (op >= 0x1b6 && op < 0x1b8)
    || (op >= 0x1bb && op < 0x1c2)
    /* sse */
    || (op >= 0x110 && op < 0x118)
    || (op >= 0x128 && op < 0x130)
    || (op == 0x150)
    || (op == 0x16e)
    || (op >= 0x174 && op < 0x177)
    || (op >= 0x17c && op < 0x180)
    /* /sse */
    || (op == 0x1a3)
    || (op == 0x1ab)
    || (op == 0x1af)
    || (op == 0x1b1)
    || (op == 0x1bc)
    || (op == 0x1be)
    /* sse */
    || (op >= 0x1d0 && op < 0x1f0)
    || (op >= 0x2f0 && op < 0x2f2))
        return get_mrm(++p, rex, mrm_rip, mrm_reg, 0);
    /* nnn without immediate */
    if((op >= 0xd0 && op < 0xd4 && (p[1] & 0x38) != 0x30)
    || (op >= 0xf6 && op < 0xf8 && (p[1] & 0x38) >= 0x10)
    || (op >= 0xfe && op < 0x100 && (p[1] & 0x38) < 0x10)
    || (op == 0xff && (p[1] & 0x38) == 0x30)
    /* sse */
    || (op >= 0x151 && op < 0x16e)
    || (op == 0x16f)
    || (op == 0x1ae)
    || (op >= 0x1f8 && op < 0x200)
    || (op == 0x200))
        return get_mrm(++p, rex, mrm_rip, mrm_reg, 1);
    /* 1-byte immediate only */
    if((op < 0x40 && (op & 7) == 4)
    || (op == 0x6a)
    || (op == 0xa8))
        return (uint64_t)(p + 2);
    /* N-byte immediate only */
    if((op < 0x40 && (op & 7) == 5)
    || (op == 0x68)
    || (op == 0xa9)
    || (op >= 0xb8 && op < 0xc0))
    {
        if((op >= 0xb8 && op < 0xc0) && (rex & 8))
            return (uint64_t)(p + 9);
        else if((rex & 16))
            return (uint64_t)(p + 3);
        else
            return (uint64_t)(p + 5);
    }
    /* single-byte */
    if((op >= 0x50 && op < 0x60)
    || (op >= 0x91 && op < 0x9a)
    || (op >= 0xa4 && op < 0xa8)
    || (op >= 0xaa && op < 0xb0)
    || (op == 0xc9)
    || (op == 0xcf)
    || (op == 0x107)
    || (op >= 0x1c8 && op < 0x1d0))
        return (uint64_t)++p;
    /* special cases */
    if(op == 0x101 && p[1] == 0xf8) //swapgs
        return (uint64_t)(p + 2);
    if(op == 0x101 && p[1] == 0xf9)
        return (uint64_t)(p + 2);
    /* yet unknown */
    return 0;
}

static uint64_t handle_lea_rip(feed_t* feed, void* opaque, uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    if((p[0] == 0x48 || p[0] == 0x4c) && p[1] == 0x8d && (p[2] & 0xc7) == 5) // lea ..., [rip+...]
    {
        //cannot handle mov the naive way: if the dereference segfaults,
        //we'd have no way to recover the original contents of the register
        int reg = (p[2] >> 3) & 7;
        uint64_t address = rip + 7 + *(int32_t*)(p + 3);
        uint8_t data[10] = {p[0] == 0x48 ? 0x48 : 0x49, 0xb8+reg};
        *(uint64_t*)(data+2) = address;
        feed(opaque, data, "\x0a\x87", 10, 2);
        return rip + 7;
    }
    if((p[0] == 0x48 || p[0] == 0x4c) && p[1] == 0x8b && (p[2] & 0xc7) == 5) // mov ..., qword [rip+...]
    {
        int reg = (p[2] >> 3) & 7;
        if(p[0] == 0x48 && reg == 4) //rsp
            return 0;
        uint64_t address = rip + 7 + *(int32_t*)(p + 3);
        uint8_t* start;
        uint8_t* start_shadow;
        if(feed == out2)
        {
            start = *(void**)opaque;
            start_shadow = jit_shadow + (start - jit_memory);
        }
        //red zone
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x00\x90", 5, 2); //lea rsp, [rsp-128]
        //push the affected register
        if(p[0] == 0x4c)
        {
            uint8_t push[2] = {0x49, 0x50 + reg};
            feed(opaque, push, "\x93", 2, 1);
        }
        else
        {
            uint8_t push = 0x50+reg;
            feed(opaque, &push, "\x93", 1, 1);
        }
        //movabs the affected register with the address
        uint8_t movabs[10] = {p[0] == 0x48 ? 0x48 : 0x49, 0xb8+reg};
        *(uint64_t*)(movabs+2) = address;
        feed(opaque, movabs, NULL, 10, 0);
        //mov r.., [r..]
        if(reg == 5) //rbp or r13
        {
            uint8_t mov[4] = {p[0] == 0x4c ? 0x4d : 0x48, 0x8b, 0x40+9*reg, 0};
            feed(opaque, mov, "\xe1\x95\x87", 4, 3);
        }
        else
        {
            uint8_t mov[4] = {p[0] == 0x4c ? 0x4d : 0x48, 0x8b, 8*reg+4, 0x20+reg};
            feed(opaque, mov, "\xe1\x95\x87", 4, 3);
        }
        //unwind the stack
        feed(opaque, "\x48\x8d\xa4\x24\x88\x00\x00\x00", "\xe5\x87", 8, 2); //lea rsp, [rsp+136]
        if(feed == out2)
        {
            uint8_t* end = *(void**)opaque;
            *start_shadow = end - start;
        }
        return rip + 7;
    }
    //other rip-relatives
    uint64_t mrm_rip;
    int mrm_reg;
    uint64_t riprel_end = get_end(rip, &mrm_rip, &mrm_reg);
    if(!riprel_end || riprel_end - rip >= 16)
        return 0;
    uint8_t instr[16];
    uint8_t* pp = (uint8_t*)rip;
    int rex = skip_prefixes(&pp);
    if(!(rex & 4) && mrm_reg == 4) // rsp
        return 0;
    if(pp[0] == 0xff && (pp[1] & 0x38) == 0x30) // push
        return 0;
    if(pp[0] == 0x8f && !(pp[1] & 0x38)) // pop
        return 0;
    uint64_t pref_end = mask_rex(instr, rip, 1, 0);
    int reg = mrm_reg == 6 ? 7 : 6;
    uint8_t* start;
    uint8_t* start_shadow;
    if(feed == out2)
    {
        start = *(void**)opaque;
        start_shadow = jit_shadow + (start - jit_memory);
    }
    //red zone
    feed(opaque, "\x48\x8d\x64\x24\x80", "\x00\x90", 5, 2); //lea rsp, [rsp-128]
    //push the affected register
    uint8_t push = 0x50+reg;
    feed(opaque, &push, "\x93", 1, 1); //push reg
    //load the affected register with absolute address
    uint64_t addr = riprel_end + *(int32_t*)mrm_rip;
    uint8_t movabs[10] = {0x48, 0xb8+reg};
    *(uint64_t*)(movabs+2) = addr;
    feed(opaque, movabs, NULL, 10, 0); //movabs reg, addr
    //the original instruction
    memcpy(instr+(pref_end-rip), (void*)pref_end, mrm_rip-pref_end);
    instr[mrm_rip-rip-1] = *(uint8_t*)(mrm_rip-1) - 5 + reg;
    memcpy(instr+(mrm_rip-rip), (void*)(mrm_rip+4), (riprel_end-mrm_rip-4));
    uint8_t shadow[2] = {0xc0 + (riprel_end - rip), 0x93};
    feed(opaque, instr, shadow, riprel_end-rip-4, 2);
    //unwind the stack
    uint8_t pop = 0x58+reg;
    feed(opaque, &pop, "\xe1", 1, 1); //pop reg
    shadow[0] = 0xe5;
    shadow[1] = 0x80 + (riprel_end - rip);
    feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", shadow, 8, 2); //lea rsp, [rsp+128]
    if(feed == out2)
    {
        uint8_t* end = *(void**)opaque;
        *start_shadow = end - start;
    }
    return riprel_end;
}

static uint64_t handle_cmov(feed_t* feed, void* opaque, uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    int rex = skip_prefixes(&p);
    if((p[0] == 0x0f && (p[1] >= 0x40 && p[1] < 0x50)) //cmov
    || (p[0] == 0x0f && (p[1] >= 0x90 && p[1] < 0xa0) && !(p[2] & 0x38))) //setcc
    {
        uint64_t end = get_mrm(p+2, rex, NULL, NULL, 0);
        if(!end || end - rip > 16)
            return 0;
        int cc = p[1] & 15;
        uint8_t* start;
        uint8_t* start_shadow;
        if(feed == out2)
        {
            start = *(void**)opaque;
            start_shadow = jit_shadow + (start - jit_memory);
        }
        //back up
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x00\x90", 5, 2); //lea rsp, [rsp-128]
        feed(opaque, "\x9c", "\x92", 1, 1); //pushfq
        feed(opaque, "\x50", "\x93", 1, 1); //push rax
        feed(opaque, "\x51", "\x93", 1, 1); //push rcx
        feed(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
        feed(opaque, (uint8_t*)&coverage, NULL, 8, 0); //...coverage
        //set<cond> cl
        uint8_t set_cond_cl[3] = {0x0f, 0x90 + cc, 0xd1};
        feed(opaque, set_cond_cl, NULL, 3, 0);
        //set<!cond> ch
        uint8_t set_not_cond_ch[3] = {0x0f, 0x90 + (cc ^ 1), 0xd5};
        feed(opaque, set_not_cond_ch, NULL, 3, 0);
        //add cl & ch to coverage map
        uint32_t hash1 = branch_hash(rip, COVERAGE_CMOV_OK);
        feed(opaque, "\xf0\x00\x88", NULL, 3, 0); //add [rax+...], cl
        feed(opaque, (uint8_t*)&hash1, NULL, 4, 0); //...hash1
        uint32_t hash2 = branch_hash(rip, COVERAGE_CMOV_FAIL);
        feed(opaque, "\xf0\x00\xa8", NULL, 3, 0); //add [rax+...], ch
        feed(opaque, (uint8_t*)&hash2, NULL, 4, 0); //...hash2
        //restore
        feed(opaque, "\x59", "\xe1", 1, 1); //pop rcx
        feed(opaque, "\x58", "\xe3", 1, 1); //pop rax
        feed(opaque, "\x9d", "\xe5", 1, 1); //popfq
        feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", "\xe7", 8, 1); //lea rsp, [rsp+128]
        //now the original instruction
        uint8_t shadow = 0x80 + end - rip;
        feed(opaque, (uint8_t*)rip, &shadow, end - rip, 1);
        if(feed == out2)
        {
            uint8_t* stop = *(void**)opaque;
            *start_shadow = stop - start;
        }
        return end;
    }
    return 0;
}

static uint64_t handle_ret(feed_t* feed, void* opaque, uint64_t rip)
{
    if(*(uint8_t*)rip == 0xc3)
    {
        //we are at function call boundary, so can trash the stack
        //no need to account for the red zone
        feed(opaque, "\x50", "\x93", 1, 1); //push rax
        feed(opaque, "\x9c", "\x92", 1, 1); //pushfq
        feed(opaque, "\xff\x15\x00\x00\x00\x00", "\x95", 6, 1); //call [rip]
        uint64_t dst = (uint64_t)asm_dispatch_ret;
        feed(opaque, (uint8_t*)&dst, NULL, 8, 0);
        return rip + 1;
    }
    return 0;
}

static uint8_t* rewrite_mrm_rsp(uint8_t* out, uint8_t* p_mrm, int rex, uint64_t sp_offset)
{
    if(!(rex & 1) && p_mrm[0] < 0xc0 && (p_mrm[0] & 7) == 4 && (p_mrm[1] & 7) == 4)
    {
        uint64_t offset = 0;
        if(p_mrm[0] >= 0x40 && p_mrm[0] < 0x80)
            offset = (int8_t)p_mrm[2];
        else if(p_mrm[0] >= 0x80 && p_mrm[0] < 0xc0)
            offset = *(int32_t*)(p_mrm+2);
        offset += sp_offset;
        if(offset == 0)
        {
            *out++ = p_mrm[0] & 0x3f;
            *out++ = p_mrm[1];
        }
        else if(offset == (int8_t)offset)
        {
            *out++ = (p_mrm[0] & 0x3f) | 0x40;
            *out++ = p_mrm[1];
            *out++ = offset;
        }
        else if(offset == (int32_t)offset)
        {
            *out++ = (p_mrm[0] & 0x3f) | 0x80;
            *out++ = p_mrm[1];
            *(int32_t*)out = offset;
            out += 4;
        }
        else
            return NULL;
        return out;
    }
    uint64_t end = get_mrm(p_mrm, rex, NULL, NULL, 0);
    if(!end)
        return NULL;
    uint8_t* end_p = (uint8_t*)end;
    memcpy(out, p_mrm, end_p-p_mrm);
    out += end_p-p_mrm;
    return out;
}

static uint64_t handle_indirect(feed_t* feed, void* opaque, uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    int rex = skip_prefixes(&p);
    if(p[0] == 0xff && (p[1] == 0x15 || p[1] == 0x25) && !(rex & 16))
    {
        if((uint64_t)p - rip >= 10)
            return 0;
        if(p[1] == 0x15)
            push_lr(feed, opaque, (uint64_t)(p+6));
        // p may be != rip in case the jump has prefixes
        uint64_t p_addr = (uint64_t)p + 6 + *(int32_t*)(p + 2);
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x90", 5, 1); //lea rsp, [rsp-128]
        feed(opaque, "\x50", "\x93", 1, 1); //push rax
        feed(opaque, "\x48\xb8", NULL, 2, 0); //movabs rax, ...
        feed(opaque, (uint8_t*)&p_addr, NULL, 8, 0); //...p_addr
        feed(opaque, "\x48\x8b\x00", NULL, 3, 0); //mov rax, [rax]
        feed(opaque, "\x50", "\x95", 1, 1); //push rax
        rip = (uint64_t)(p+6);
    }
    else if(p[0] == 0xff && ((p[1] & 0xf8) == 0xd0 || (p[1] & 0xf8) == 0xe0) && !(rex & 16))
    {
        if((!(rex & 1) && (p[1] & 7) == 4) || (uint64_t)p - rip >= 14)
            return 0;
        if((p[1] & 0xf8) == 0xd0)
            push_lr(feed, opaque, (uint64_t)(p+2));
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x90", 5, 1); //lea rsp, [rsp-128]
        feed(opaque, "\x50", "\x93", 1, 1); //push rax
        if((rex & 1))
            feed(opaque, "\x49", NULL, 1, 0); //rex
        uint8_t push = 0x50+(p[1]&7);
        feed(opaque, &push, "\xc5", 1, 1); //push <reg>
        rip = (uint64_t)(p+2);
    }
    else if(p[0] == 0xff && ((p[1] & 0x38) == 0x10 || (p[1] & 0x38) == 0x20) && !(rex & 16))
    {
        uint64_t lr = get_mrm(p+1, rex, NULL, NULL, 0);
        if(lr - rip >= 16)
            return 0;
        if((p[1] & 0x38) == 0x10)
            push_lr(feed, opaque, lr);
        feed(opaque, "\x48\x8d\x64\x24\x80", "\x90", 5, 1); //lea rsp, [rsp-128]
        feed(opaque, "\x50", "\x93", 1, 1); //push rax
        uint8_t instr[32];
        uint8_t* pp = instr + (mask_rex(instr, rip, 4, 0) - rip);
        *pp++ = 0xff;
        uint8_t* p_mrm = pp;
        pp = rewrite_mrm_rsp(pp, p+1, rex, (p[1] & 0x38) == 0x10 ? 144 : 136);
        p_mrm[0] = (p_mrm[0] & 0xc7) | 0x30;
        feed(opaque, instr, "\x95", pp-instr, 0); //push [...]
        rip = lr;
    }
    else
        return 0;
    feed(opaque, "\x9c", "\x92", 1, 1); //pushfq
    feed(opaque, "\xff\x15\x00\x00\x00\x00", NULL, 6, 0); //call [rip]
    uint64_t adi = (uint64_t)asm_dispatch_indirect;
    feed(opaque, (uint8_t*)&adi, NULL, 8, 0);
    return rip;
}

static uint64_t is_nop(uint64_t rip)
{
    uint8_t* p = (uint8_t*)rip;
    int rex = skip_prefixes(&p);
    if(p[0] == 0x90)
        return (uint64_t)(p + 1);
    else if(p[0] == 0x0f && p[1] == 0x1f)
    {
        uint64_t mrm_rip;
        int mrm_reg;
        return get_mrm(p+2, rex, &mrm_rip, &mrm_reg, 1);
    }
    return 0;
}

static int is_magic(uint64_t rip)
{
    return !memcmp((void*)rip, "\x0f\x1f\x25\x78\x56\x34\x12", 7);
}

#ifdef TRACE

int trace(uint64_t* trace_frame)
{
    int enter = tinycov_hal_enter();
#ifndef TRACE_SILENT
    char buf[4096];
    char* p = buf;
    p += sprintf(p, "rip = 0x%016llx flags=0x%llx\n", *(uint64_t*)(trace_frame[17]+10), trace_frame[16]);
    p += sprintf(p, "rax = 0x%016llx rcx = 0x%016llx\n", trace_frame[15], trace_frame[14]);
    p += sprintf(p, "rdx = 0x%016llx rbx = 0x%016llx\n", trace_frame[13], trace_frame[12]);
    p += sprintf(p, "rsp = 0x%016llx rbp = 0x%016llx\n", trace_frame[11]+176, trace_frame[10]);
    p += sprintf(p, "rsi = 0x%016llx rdi = 0x%016llx\n", trace_frame[9], trace_frame[8]);
    p += sprintf(p, "r8  = 0x%016llx r9  = 0x%016llx\n", trace_frame[7], trace_frame[6]);
    p += sprintf(p, "r10 = 0x%016llx r11 = 0x%016llx\n", trace_frame[5], trace_frame[4]);
    p += sprintf(p, "r12 = 0x%016llx r13 = 0x%016llx\n", trace_frame[3], trace_frame[2]);
    p += sprintf(p, "r14 = 0x%016llx r15 = 0x%016llx\n", trace_frame[1], trace_frame[0]);
    p += sprintf(p, "[rsp]=0x%016llx\n", *(uint64_t*)(trace_frame[11]+176));
    p += sprintf(p, "[rip]=");
    for(int i = 0; i < 16; i++)
        p += sprintf(p, " %02hhx", *(uint8_t*)(i+*(uint64_t*)(trace_frame[17]+10)));
    p += sprintf(p, "\n");
    char xmm[32];
#define XMM(i)\
    asm("vmovups %%ymm" #i ", (%0)"::"r"(&xmm));\
    p += sprintf(p, "ymm" #i " =");\
    for(int j = 0; j < 32; j++)\
        p += sprintf(p, " %02hhx", xmm[j]);\
    *p++ = '\n';
    XMM(0) XMM(1) XMM(2) XMM(3) XMM(4) XMM(5) XMM(6) XMM(7)
    XMM(8) XMM(9) XMM(10) XMM(11) XMM(12) XMM(13) XMM(14) XMM(15)
#undef XMM
    int tmp = errno;
#ifdef DEBUG_AFTER
    if(counter >= DEBUG_AFTER)
#endif
        tinycov_debug_write(2, buf, p - buf);
    errno = tmp;
#endif
#if defined(DEBUG_AFTER) || defined(MAX_COUNT)
    counter++;
#endif
    tinycov_hal_exit(enter);
#ifdef MAX_COUNT
    if(counter >= MAX_COUNT)
        return 1;
#endif
    return 0;
}

asm(
".section .text\n"
"asm_trace:\n"
"pushfq\n"
"push %rax\n"
"push %rcx\n"
"push %rdx\n"
"push %rbx\n"
"push %rsp\n"
"push %rbp\n"
"push %rsi\n"
"push %rdi\n"
"push %r8\n"
"push %r9\n"
"push %r10\n"
"push %r11\n"
"push %r12\n"
"push %r13\n"
"push %r14\n"
"push %r15\n"
"mov %rsp, %rbp\n"
"mov %rsp, %rdi\n"
"and $0xfffffffffffffff0, %rsp\n"
"call trace\n"
"mov %rbp, %rsp\n"
"pop %r15\n"
"pop %r14\n"
"pop %r13\n"
"pop %r12\n"
"pop %r11\n"
"pop %r10\n"
"pop %r9\n"
"pop %r8\n"
"pop %rdi\n"
"pop %rsi\n"
"pop %rbp\n"
"pop %rsp\n"
"pop %rbx\n"
"pop %rdx\n"
"pop %rcx\n"
#ifdef MAX_COUNT
"test %rax, %rax\n"
"jz .L1\n"
"mov 16(%rsp), %rax\n"
"mov 10(%rax), %rax\n"
"mov %rax, 16(%rsp)\n"
"pop %rax\n"
"popfq\n"
"ret $128\n"
".L1:\n"
#endif
"pop %rax\n"
"popfq\n"
"ret\n"
);

extern char asm_trace[];

#endif

/* return values:
* 0 = compiled successfully
* 1 = failed to compile
* 2 = compiled successfully, jumps
*/
static int compile_instr(uint64_t* p_rip, feed_t* feed, void* opaque)
{
    uint64_t rip = *p_rip;
    DEBUG("in compile_instr, rip=0x%016lx\n", rip);
    //best effort checks. don't rely on their correctness
    if(rip >= (uint64_t)jit_memory && rip < (uint64_t)(jit_memory + JIT_MEMORY_SIZE))
    {
        DEBUG("FATAL: compile_instr() called for address within the jit region\n");
        for(;;)
            asm volatile("");
    }
#ifdef KERNEL
    if(rip >= (uint64_t)protect_start && rip < (uint64_t)protect_end)
    {
        DEBUG("FATAL: compile_instr() called for address within tinycov itself\n");
        for(;;)
            asm volatile("");
    }
#endif
#ifdef TRACE
#ifdef DEBUG_AFTER
    if(counter >= DEBUG_AFTER)
#endif
    {
        feed(opaque, "\x48\x8d\x64\x24\x80", NULL, 5, 0); //lea rsp, [rsp-128]
        feed(opaque, "\xff\x15\x02\x00\x00\x00", NULL, 6, 0); //call [rip+2]
        feed(opaque, "\xeb\x10", NULL, 2, 0); //jmp rip+16
        uint64_t trace_addr = (uint64_t)asm_trace;
        feed(opaque, (uint8_t*)&trace_addr, NULL, 8, 0);
        feed(opaque, (uint8_t*)&rip, NULL, 8, 0);
        feed(opaque, "\x48\x8d\xa4\x24\x80\x00\x00\x00", NULL, 8, 0); //lea rsp, [rsp+128]
    }
#endif
    if(is_magic(rip)) //magic exit instruction
    {
        DEBUG("-> is magic\n");
        uint8_t buf[14] = {0xff, 0x25};
        *(uint64_t*)(buf + 6) = rip;
        uint8_t shadow = 14;
        feed(opaque, buf, &shadow, 14, 1);
        *p_rip = rip + 7;
        return 2;
    }
    /* can we copy it as is? */
    uint64_t end = get_end(rip, NULL, NULL);
    if(end && end - rip >= 16) //too long, won't even bother
    {
        DEBUG("-> is too long\n");
        return 1;
    }
    if(end)
    {
        DEBUG("-> is simple\n");
        uint8_t shadow[2] = {end - rip, end - rip + 128};
        feed(opaque, (uint8_t*)rip, shadow, end - rip, 2);
        *p_rip = end;
        return 0;
    }
    /* jumps */
    uint64_t dst;
    int cc;
    end = parse_jump(rip, &cc, &dst);
    if(end)
    {
        DEBUG("-> is an unconditional jump\n");
        handle_jump(feed, opaque, cc, end, dst);
        *p_rip = end;
        return 2;
    }
    end = parse_condjump(rip, &cc, &dst);
    if(end)
    {
        DEBUG("-> is a conditional jump\n");
        handle_condjump(feed, opaque, cc, rip, dst, end-rip);
        *p_rip = end;
        return 0;
    }
    /* cmov */
    end = handle_cmov(feed, opaque, rip);
    if(end)
    {
        DEBUG("-> is a cmov\n");
        *p_rip = end;
        return 0;
    }
    /* lea(%rip) */
    end = handle_lea_rip(feed, opaque, rip);
    if(end)
    {
        DEBUG("-> is a rip-relative\n");
        *p_rip = end;
        return 0;
    }
    /* nop */
    end = is_nop(rip);
    if(end && end - rip >= 16)
    {
        DEBUG("-> is too long\n");
        return 1;
    }
    if(end)
    {
        DEBUG("-> is nop\n");
        //would be ideal to skip it altogether
        //but need to have at least 1 byte for unwinding to work
        uint8_t shadow = 0xd0 + (end - rip);
        feed(opaque, "\x90", &shadow, 1, 1);
        *p_rip = end;
        return 0;
    }
    /* ret */
    end = handle_ret(feed, opaque, rip);
    if(end)
    {
        DEBUG("-> is ret\n");
        *p_rip = end;
        return 2;
    }
    /* indirect jumps */
    end = handle_indirect(feed, opaque, rip);
    if(end)
    {
        DEBUG("-> is indirect jump\n");
        *p_rip = end;
        return 2;
    }
    /* syscall */
    end = handle_syscall(feed, opaque, rip);
    if(end)
    {
        DEBUG("-> is syscall\n");
        *p_rip = end;
        return 0;
    }
    /* yet unknown */
    DEBUG("-> unknown\n");
    return 1;
}

static void* compile(uint64_t rip, int stub_on_fail)
{
    uint64_t end = rip;
    size_t alloc_sz = 0;
    void* tr;
    int status;
    int any = 0;
    DEBUG("compile() pass 1\n");
    while(!(status = compile_instr(&end, feed_increment, &alloc_sz)) && !(tr = get_translated(end, 1)))
        any = 1;
    if(status == 1 && !any && !stub_on_fail) //failed to compile anything
        return (void*)2;
    if(tr == (void*)2)
        status = 1;
    if(status == 1)
    {
        alloc_sz++; //0xcc trap
        alloc_sz += 8; //real rip
    }
    else if(!status)
        alloc_sz += 5; //relative jump
    void* ans = alloc_aligned(alloc_sz+9, 1);
    uint8_t shadow0[9] = {0};
    *(uint64_t*)shadow0 = rip;
    shadow0[8] = 0xff;
    out2(&ans, shadow0, shadow0, 9, 9);
    void* buf = ans;
    uint64_t i = rip;
    DEBUG("compile() pass 2\n");
    if(i != end)
        compile_instr(&i, out2, &buf);
    while(i != end)
    {
        void* bb = buf;
        uint64_t j = i;
        compile_instr(&i, out2, &buf);
        set_translated(j, bb);
    }
    if(status != 2)
        set_translated(i, (void*)2);
    if(status == 1)
    {
        out2(&buf, "\xcc", "\x91", 1, 1);
        out2(&buf, (void*)&end, NULL, 8, 0);
    }
    else if(!status)
    {
        uint8_t* src = buf + 5;
        int32_t delta = (uint8_t*)tr - src;
        uint8_t data[5];
        data[0] = 0xe9;
        *(int32_t*)(data+1) = delta;
        uint8_t shadow = 5;
        out2(&buf, data, &shadow, 5, 1);
    }
    DEBUG("compile() = %p\n", ans);
    return ans;
}

static void jit_in(struct trap_state* uc)
{
    uint64_t rip = uc->regs[TRAP_REG_RIP];
    void* tr = get_translated(rip, 1);
    if(!tr)
    {
        tr = compile(rip, 0);
        set_translated(rip, tr);
    }
    if(tr == (void*)2 /* failed translation */ || *(uint8_t*)tr == 0xcc /* stub translations */)
        uc->regs[TRAP_REG_EFLAGS] |= 256;
    else
        uc->regs[TRAP_REG_RIP] = (uint64_t)tr;
}

#ifdef TEST_MODE
int hits[256];
uint64_t last_hit[256];
int hits2[256];
uint64_t last_hit2[256];
#endif

static void sigtrap_handler(struct trap_state* uc)
{
    int enter = tinycov_hal_enter();
    DEBUG("sigtrap caught!\n");
    jit_out(uc);
    DEBUG("rip=0x%016lx\n", uc->regs[TRAP_REG_RIP]);
    jit_in(uc);
    DEBUG("translated rip=0x%016lx\n", uc->regs[TRAP_REG_RIP]);
#ifdef TEST_MODE
    if((uc->regs[TRAP_REG_EFLAGS] & 256))
    {
        uint8_t* rip = (uint8_t*)uc->regs[TRAP_REG_RIP];
        if(*rip >= 0x40 && *rip < 0x50)
            rip++;
        hits[*rip]++;
        last_hit[*rip] = (uint64_t)rip;
        if(*rip == 0x0f)
        {
            hits2[rip[1]]++;
            last_hit2[rip[1]] = (uint64_t)rip;
        }
    }
#endif
#ifdef DEBUG_PRINTS
    if((uc->regs[TRAP_REG_EFLAGS] & 256))
    {
        uint8_t* rip = (uint8_t*)uc->regs[TRAP_REG_RIP];
        DEBUG("failed instruction was:");
        for(int i = 0; i < 16; i++)
            DEBUG(" %02x", *rip++);
        DEBUG("\n");
    }
#endif
    tinycov_hal_exit(enter);
}

void tinycov_setup(void)
{
    coverage = tinycov_hal_get_coverage_shmem();
    DEBUG("coverage = %p\n", coverage);
    jit_memory = tinycov_hal_map_memory((size_t)JIT_MEMORY_SIZE*2+sizeof(*p_jit_state));
    jit_shadow = jit_memory + JIT_MEMORY_SIZE;
    p_jit_state = (void*)jit_shadow + JIT_MEMORY_SIZE;
    DEBUG("jit_memory = %p, jit_shadow = %p, p_jit_state = %p\n", jit_memory, jit_shadow, p_jit_state);
    tinycov_hal_set_handler(TRAP_INT1, sigtrap_handler);
    DEBUG("calling midsetup...\n");
    tinycov_hal_midsetup();
    asm volatile("pop %%rcx\npushfq\norb $1, 1(%%rsp)\npopfq\npush %%rcx":::"rcx");
}

void* tinycov_entry(void* addr)
{
    DEBUG("tinycov_entry(%p) called\n", addr);
    uint64_t dst0 = (uint64_t)addr;
    void* dst_p = get_translated(dst0, 1);
    if(!dst_p || dst_p == (void*)2 /* hack: overwrite already saved stub */)
    {
        dst_p = compile(dst0, 1);
        set_translated(dst0, dst_p);
    }
    DEBUG("tinycov_entry(%p) = %p\n", addr, dst_p);
    return dst_p;
}

asm(".section .text\nprotect_end:\n");
